<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Encuesta;
use AppBundle\Form\EncuestaType;

/**
 * Encuesta controller.
 *
 * @Route("/encuesta")
 */
class EncuestaController extends Controller
{
    /**
     * Lists all Encuesta entities.
     *
     * @Route("/", name="encuesta_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $encuestas = $em->getRepository('AppBundle:Encuesta')->findAll();

        return $this->render('encuesta/index.html.twig', array(
            'encuestas' => $encuestas,
        ));
    }

    /**
     * Creates a new Encuesta entity.
     *
     * @Route("/new", name="encuesta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $encuestum = new Encuesta();
        $form = $this->createForm('AppBundle\Form\EncuestaType', $encuestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($encuestum);
            $em->flush();

            return $this->redirectToRoute('encuesta_show', array('id' => $encuestum->getEncuestaId()));
        }

        return $this->render('encuesta/new.html.twig', array(
            'encuestum' => $encuestum,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Encuesta entity.
     *
     * @Route("/{id}", name="encuesta_show")
     * @Method("GET")
     */
    public function showAction(Encuesta $encuestum)
    {
        $deleteForm = $this->createDeleteForm($encuestum);

        return $this->render('encuesta/show.html.twig', array(
            'encuestum' => $encuestum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Encuesta entity.
     *
     * @Route("/{id}/edit", name="encuesta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Encuesta $encuestum)
    {
        $deleteForm = $this->createDeleteForm($encuestum);
        $editForm = $this->createForm('AppBundle\Form\EncuestaType', $encuestum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($encuestum);
            $em->flush();

            return $this->redirectToRoute('encuesta_edit', array('id' => $encuestum->getEncuestaId()));
        }

        return $this->render('encuesta/edit.html.twig', array(
            'encuestum' => $encuestum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Encuesta entity.
     *
     * @Route("/{id}", name="encuesta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Encuesta $encuestum)
    {
        $form = $this->createDeleteForm($encuestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($encuestum);
            $em->flush();
        }

        return $this->redirectToRoute('encuesta_index');
    }

    /**
     * Creates a form to delete a Encuesta entity.
     *
     * @param Encuesta $encuestum The Encuesta entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Encuesta $encuestum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('encuesta_delete', array('id' => $encuestum->getEncuestaId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
