<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Audiometria;
use AppBundle\Entity\MedidasAerea;
use AppBundle\Entity\MedidasOsea;

/**
 * Audiometria controller.
 *
 * @Route("/audiometria")
 */
class AudiometriaController extends Controller
{
    /**
     * Lists all Audiometria entities.
     *
     * @Route("/", name="audiometria_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $audiometrias = $em->getRepository('AppBundle:Audiometria')->findAll();

        return $this->render('audiometria/index.html.twig', array(
            'audiometrias' => $audiometrias,
        ));
    }

    /**
     * render audiometria form.
     *
     * @Route("/new_form", name="audiometria_new_form")
     * @Method({"GET", "POST"})
     */
    public function newFormAction(Request $request)
    {

        $persona_id = $request->get('id');

        $persona = $this->getDoctrine()
            ->getRepository('AppBundle:Persona')
            ->find($persona_id);

        $audiometrias = $persona->getAudiometrias();

        if ($audiometrias->isEmpty()) {
            $nombreAudiometria = 'Base';
        }else{

            //obtengo la ultima audiometria
            $ultimaAudiometria = $audiometrias->last();

            if ($ultimaAudiometria->getAudiometriaNombre() == 'Base') {
               $nombreAudiometria = 'Seguimiento';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'NORMAL' && $ultimaAudiometria->getAudiometriaNombre() == 'Seguimiento') {
               $nombreAudiometria = 'Seguimiento';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'ALTERADA' && $ultimaAudiometria->getAudiometriaNombre() == 'Seguimiento') {
               $nombreAudiometria = 'Comprobacion';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'ALTERADA' && $ultimaAudiometria->getAudiometriaNombre() == 'Comprobacion') {
               $nombreAudiometria = 'Confirmacion';
            }
            
        }

        return $this->render('audiometria/new.html.twig', array(
            'persona' => $persona_id,
            'nombreAudiometria' => $nombreAudiometria,
            )
        );
    }

    /**
     * Creates a new Audiometria entity.
     *
     * @Route("/new", name="audiometria_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $persona_id = $request->get('persona_id');
        $persona = $this->getDoctrine()
            ->getRepository('AppBundle:Persona')
            ->find($persona_id);

        $em = $this->getDoctrine()->getManager();

        $audiometria  = new Audiometria();

        $nombreAudiometria = $request->get('nombre_audiometria');
        $audiometria->setAudiometriaNombre($nombreAudiometria);
        $audiometria->setPersona($persona);

        $audiometria->setAudiometriaOtoscopiaOi($request->get('audiometriaOtoscopiaOi'));
        $audiometria->setAudiometriaOtoscopiaOd($request->get('audiometriaOtoscopiaOd'));

        $audiometria->setAudiometriaObservaciones($request->get('audiometriaObservaciones'));

        $em->persist($audiometria);
        $em->flush();

        $audiometriaId = $audiometria->getAudiometriaId();

        $medidas = $this->agregarMedidasAudiometria($audiometriaId, $request);
        $em->flush();

        //comparacion para las alertas
        $audiometrias = $persona->getAudiometrias();
        $cantidadAudiometrias = $audiometrias->count();
        
        if ($cantidadAudiometrias > 1) {
            $audiometria1  = $audiometrias->last();

            //obtengo las medidas aereas de la audiometria 1
            $medidasAereaA1 = $audiometria1->getMedidasAerea();

            foreach ($medidasAereaA1 as $medida) {
                if ($medida->getMedidasAereaOido() == 'IZQUIERDO') {
                    $medidasAereaOiA1 = $medida;
                }
                if ($medida->getMedidasAereaOido() == 'DERECHO') {
                    $medidasAereaOdA1 = $medida;
                }
            }

            $audiometria2  = $audiometrias->get($cantidadAudiometrias-2);

            //obtengo las medidas aereas de la audiometria 2
            $medidasAereaA2 = $audiometria2->getMedidasAerea();

            foreach ($medidasAereaA2 as $medida) {
                if ($medida->getMedidasAereaOido() == 'IZQUIERDO') {
                    $medidasAereaOiA2 = $medida;
                }
                if ($medida->getMedidasAereaOido() == 'DERECHO') {
                    $medidasAereaOdA2 = $medida;
                }
            }

            //$alerta = $medidasAereaOiA1->getMedidasAerea250() - $medidasAereaOiA2->getMedidasAerea250();
            //compara la perdida de 15 db en cualquier frecuencia aerea
            if ($medidasAereaOiA1->getMedidasAerea250() - $medidasAereaOiA2->getMedidasAerea250() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea500() - $medidasAereaOiA2->getMedidasAerea500() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea1000() - $medidasAereaOiA2->getMedidasAerea1000() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea2000() - $medidasAereaOiA2->getMedidasAerea2000() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea3000() - $medidasAereaOiA2->getMedidasAerea3000() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea4000() - $medidasAereaOiA2->getMedidasAerea4000() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea6000() - $medidasAereaOiA2->getMedidasAerea6000() >= 15 ||
                $medidasAereaOiA1->getMedidasAerea8000() - $medidasAereaOiA2->getMedidasAerea8000() >= 15 ||

                $medidasAereaOdA1->getMedidasAerea250() - $medidasAereaOdA2->getMedidasAerea250() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea500() - $medidasAereaOdA2->getMedidasAerea500() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea1000() - $medidasAereaOdA2->getMedidasAerea1000() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea2000() - $medidasAereaOdA2->getMedidasAerea2000() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea3000() - $medidasAereaOdA2->getMedidasAerea3000() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea4000() - $medidasAereaOdA2->getMedidasAerea4000() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea6000() - $medidasAereaOdA2->getMedidasAerea6000() >= 15 ||
                $medidasAereaOdA1->getMedidasAerea8000() - $medidasAereaOdA2->getMedidasAerea8000() >= 15 

                ) {
                $audiometria->setAudiometriaEstado('ALTERADA');
                $em->flush();
                $alerta = 'Debe realizar una audiometria de comprobacion de inmediato';
            }

        }
             
        if ($medidas == true) {
            //$cedula = $persona->getPersonaCedula();
            //return $this->redirectToRoute('event_new',array('exposicionRuido' => $exposicionRuido, 'cedula' => $cedula));
            if (isset($alerta)) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('alerta', $alerta);
            }
            
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Audiometria Guardada Exitosamente!');
            return $this->redirectToRoute('audiometria_show', array('id' => $audiometria->getAudiometriaId()));

        }

        
    }

    /**
     * Finds and displays a Audiometria entity.
     *
     * @Route("/{id}", name="audiometria_show")
     * @Method("GET")
     */
    public function showAction(Audiometria $audiometrium)
    {
        $deleteForm = $this->createDeleteForm($audiometrium);

        $em = $this->getDoctrine()->getManager();
        $persona = $audiometrium->getPersona();

        
        $medidasOsea = $audiometrium->getMedidasOsea();
        $medidasAerea = $audiometrium->getMedidasAerea();

        foreach ($medidasOsea as $medida) {
            if ($medida->getMedidasOseaOido() == 'IZQUIERDO') {
                $medidasOseaOi = $medida;

            }
            if ($medida->getMedidasOseaOido() == 'DERECHO') {
                $medidasOseaOd = $medida;
            }
        }
        foreach ($medidasAerea as $medida) {
            if ($medida->getMedidasAereaOido() == 'IZQUIERDO') {
                $medidasAereaOi = $medida;
            }
            if ($medida->getMedidasAereaOido() == 'DERECHO') {
                $medidasAereaOd = $medida;
            }
        }

        $observaciones = $audiometrium->getAudiometriaObservaciones();

        /**
        * Analizis del oido Izquierdo
        * Existe un diagnóstico audiológico de audicion normal cuando:
        **/

        //la via osea y la via aerea no cae por debajo de los 20
        if ($medidasOseaOi->getMedidasOsea250() < 20 &&
            $medidasOseaOi->getMedidasOsea500() < 20 &&
            $medidasOseaOi->getMedidasOsea1000() < 20 &&
            $medidasOseaOi->getMedidasOsea2000() < 20 &&
            $medidasOseaOi->getMedidasOsea3000() < 20 &&
            $medidasOseaOi->getMedidasOsea4000() < 20 &&

            $medidasAereaOi->getMedidasAerea250() < 20 &&
            $medidasAereaOi->getMedidasAerea500() < 20 &&
            $medidasAereaOi->getMedidasAerea1000() < 20 &&
            $medidasAereaOi->getMedidasAerea2000() < 20 &&
            $medidasAereaOi->getMedidasAerea3000() < 20 &&
            $medidasAereaOi->getMedidasAerea4000() < 20
            ) {
            $tipoPerdidaOi = 'NINGUNA';
        }

        /**
        * Existe un diagnóstico audiológico de HA‐C cuando:
        **/
        
        //La audición por VO se encuentra en rangos normales.
        if ($medidasOseaOi->getMedidasOsea250() < 20 &&
            $medidasOseaOi->getMedidasOsea500() < 20 &&
            $medidasOseaOi->getMedidasOsea1000() < 20 &&
            $medidasOseaOi->getMedidasOsea2000() < 20 &&
            $medidasOseaOi->getMedidasOsea3000() < 20 &&
            $medidasOseaOi->getMedidasOsea4000() < 20) {

            //La audición por VA esta alterada (bajo los 20dB).
            if ($medidasAereaOi->getMedidasAerea250() > 20 ||
                $medidasAereaOi->getMedidasAerea500() > 20 ||
                $medidasAereaOi->getMedidasAerea1000() > 20 ||
                $medidasAereaOi->getMedidasAerea2000() > 20 ||
                $medidasAereaOi->getMedidasAerea3000() > 20 ||
                $medidasAereaOi->getMedidasAerea4000() > 20) {

                //hay presencia de un "GAP ósteo‐aéreo" superior a 15dB.
                if ($medidasAereaOi->getMedidasAerea250() - $medidasOseaOi->getMedidasOsea250() > 15 ||
                    $medidasAereaOi->getMedidasAerea500() - $medidasOseaOi->getMedidasOsea500() > 15 ||
                    $medidasAereaOi->getMedidasAerea1000() - $medidasOseaOi->getMedidasOsea1000() > 15 ||
                    $medidasAereaOi->getMedidasAerea2000() - $medidasOseaOi->getMedidasOsea2000() > 15 ||
                    $medidasAereaOi->getMedidasAerea3000() - $medidasOseaOi->getMedidasOsea3000() > 15 ||
                    $medidasAereaOi->getMedidasAerea4000() - $medidasOseaOi->getMedidasOsea4000() > 15 ) {

                    $tipoPerdidaOi = 'CONDUCTIVA';
                }
            }
        }


        /**
        * Existe un diagnóstico audiológico de HA‐SN cuando:
        **/
        
        //La audición por VO esta alterada (bajo los 20dB).
        if ($medidasAereaOi->getMedidasAerea250() > 20 ||
            $medidasAereaOi->getMedidasAerea500() > 20 ||
            $medidasAereaOi->getMedidasAerea1000() > 20 ||
            $medidasAereaOi->getMedidasAerea2000() > 20 ||
            $medidasAereaOi->getMedidasAerea3000() > 20 ||
            $medidasAereaOi->getMedidasAerea4000() > 20) {

            //La audición por VA esta alterada (bajo los 20dB).
            if ($medidasAereaOi->getMedidasAerea250() > 20 ||
                $medidasAereaOi->getMedidasAerea500() > 20 ||
                $medidasAereaOi->getMedidasAerea1000() > 20 ||
                $medidasAereaOi->getMedidasAerea2000() > 20 ||
                $medidasAereaOi->getMedidasAerea3000() > 20 ||
                $medidasAereaOi->getMedidasAerea4000() > 20) {

                //hay AUSENCIA de GAP ósteo‐aéreo, o éste es inferior a 15dB.
                if ($medidasAereaOi->getMedidasAerea250() - $medidasOseaOi->getMedidasOsea250() < 15 ||
                    $medidasAereaOi->getMedidasAerea500() - $medidasOseaOi->getMedidasOsea500() < 15 ||
                    $medidasAereaOi->getMedidasAerea1000() - $medidasOseaOi->getMedidasOsea1000() < 15 ||
                    $medidasAereaOi->getMedidasAerea2000() - $medidasOseaOi->getMedidasOsea2000() < 15 ||
                    $medidasAereaOi->getMedidasAerea3000() - $medidasOseaOi->getMedidasOsea3000() < 15 ||
                    $medidasAereaOi->getMedidasAerea4000() - $medidasOseaOi->getMedidasOsea4000() < 15 ) {

                    $tipoPerdidaOi = 'SENSORIAL';
                }
            }
        }

        /**
        * Existe un diagnóstico audiológico de HA‐Mx cuando:
        **/
        
        //La audición por VO esta alterada (bajo los 20dB).
        if ($medidasOseaOd->getMedidasOsea250() > 20 ||
            $medidasOseaOd->getMedidasOsea500() > 20 ||
            $medidasOseaOd->getMedidasOsea1000() > 20 ||
            $medidasOseaOd->getMedidasOsea2000() > 20 ||
            $medidasOseaOd->getMedidasOsea3000() > 20 ||
            $medidasOseaOd->getMedidasOsea4000() > 20) {

            //La audición por VA esta alterada (bajo los 20dB).
            if ($medidasAereaOd->getMedidasAerea250() > 20 ||
                $medidasAereaOd->getMedidasAerea500() > 20 ||
                $medidasAereaOd->getMedidasAerea1000() > 20 ||
                $medidasAereaOd->getMedidasAerea2000() > 20 ||
                $medidasAereaOd->getMedidasAerea3000() > 20 ||
                $medidasAereaOd->getMedidasAerea4000() > 20) {

                //hay AUSENCIA de GAP ósteo‐aéreo, o éste es superior a 15dB.
                if ($medidasAereaOd->getMedidasAerea250() - $medidasOseaOd->getMedidasOsea250() > 15 ||
                    $medidasAereaOd->getMedidasAerea500() - $medidasOseaOd->getMedidasOsea500() > 15 ||
                    $medidasAereaOd->getMedidasAerea1000() - $medidasOseaOd->getMedidasOsea1000() > 15 ||
                    $medidasAereaOd->getMedidasAerea2000() - $medidasOseaOd->getMedidasOsea2000() > 15 ||
                    $medidasAereaOd->getMedidasAerea3000() - $medidasOseaOd->getMedidasOsea3000() > 15 ||
                    $medidasAereaOd->getMedidasAerea4000() - $medidasOseaOd->getMedidasOsea4000() > 15 ) {

                    $tipoPerdidaOd = 'MIXTA';
                }
            }
        }

        /**
        *  Fin Analizis del oido Izquierdo
        **/

        /**
        * Analizis del oido Derecho
        * Existe un diagnóstico audiológico de audicion normal cuando:
        **/

        //la via osea y la via aerea no cae por debajo de los 20
        if ($medidasOseaOd->getMedidasOsea250() < 20 &&
            $medidasOseaOd->getMedidasOsea500() < 20 &&
            $medidasOseaOd->getMedidasOsea1000() < 20 &&
            $medidasOseaOd->getMedidasOsea2000() < 20 &&
            $medidasOseaOd->getMedidasOsea3000() < 20 &&
            $medidasOseaOd->getMedidasOsea4000() < 20 &&

            $medidasAereaOd->getMedidasAerea250() < 20 &&
            $medidasAereaOd->getMedidasAerea500() < 20 &&
            $medidasAereaOd->getMedidasAerea1000() < 20 &&
            $medidasAereaOd->getMedidasAerea2000() < 20 &&
            $medidasAereaOd->getMedidasAerea3000() < 20 &&
            $medidasAereaOd->getMedidasAerea4000() < 20
            ) {
            $tipoPerdidaOd = 'NINGUNA';
        }

        /**
        * Existe un diagnóstico audiológico de HA‐C cuando:
        **/
        
        //La audición por VO se encuentra en rangos normales.
        if ($medidasOseaOd->getMedidasOsea250() < 20 &&
            $medidasOseaOd->getMedidasOsea500() < 20 &&
            $medidasOseaOd->getMedidasOsea1000() < 20 &&
            $medidasOseaOd->getMedidasOsea2000() < 20 &&
            $medidasOseaOd->getMedidasOsea3000() < 20 &&
            $medidasOseaOd->getMedidasOsea4000() < 20) {

            //La audición por VA estará alterada (bajo los 20dB).
            if ($medidasAereaOd->getMedidasAerea250() > 20 ||
                $medidasAereaOd->getMedidasAerea500() > 20 ||
                $medidasAereaOd->getMedidasAerea1000() > 20 ||
                $medidasAereaOd->getMedidasAerea2000() > 20 ||
                $medidasAereaOd->getMedidasAerea3000() > 20 ||
                $medidasAereaOd->getMedidasAerea4000() > 20) {

                //hay presencia de un "GAP ósteo‐aéreo" superior a 15dB.
                if ($medidasAereaOd->getMedidasAerea250() - $medidasOseaOd->getMedidasOsea250() > 15 ||
                    $medidasAereaOd->getMedidasAerea500() - $medidasOseaOd->getMedidasOsea500() > 15 ||
                    $medidasAereaOd->getMedidasAerea1000() - $medidasOseaOd->getMedidasOsea1000() > 15 ||
                    $medidasAereaOd->getMedidasAerea2000() - $medidasOseaOd->getMedidasOsea2000() > 15 ||
                    $medidasAereaOd->getMedidasAerea3000() - $medidasOseaOd->getMedidasOsea3000() > 15 ||
                    $medidasAereaOd->getMedidasAerea4000() - $medidasOseaOd->getMedidasOsea4000() > 15 ) {

                    $tipoPerdidaOd = 'CONDUCTIVA';
                }
            }
        }


        /**
        * Existe un diagnóstico audiológico de HA‐SN cuando:
        **/
        
        //La audición por VO esta alterada (bajo los 20dB).
        if ($medidasOseaOd->getMedidasOsea250() > 20 ||
            $medidasOseaOd->getMedidasOsea500() > 20 ||
            $medidasOseaOd->getMedidasOsea1000() > 20 ||
            $medidasOseaOd->getMedidasOsea2000() > 20 ||
            $medidasOseaOd->getMedidasOsea3000() > 20 ||
            $medidasOseaOd->getMedidasOsea4000() > 20) {

            //La audición por VA esta alterada (bajo los 20dB).
            if ($medidasAereaOd->getMedidasAerea250() > 20 ||
                $medidasAereaOd->getMedidasAerea500() > 20 ||
                $medidasAereaOd->getMedidasAerea1000() > 20 ||
                $medidasAereaOd->getMedidasAerea2000() > 20 ||
                $medidasAereaOd->getMedidasAerea3000() > 20 ||
                $medidasAereaOd->getMedidasAerea4000() > 20) {

                //hay AUSENCIA de GAP ósteo‐aéreo, o éste es inferior a 15dB.
                if ($medidasAereaOd->getMedidasAerea250() - $medidasOseaOd->getMedidasOsea250() < 15 ||
                    $medidasAereaOd->getMedidasAerea500() - $medidasOseaOd->getMedidasOsea500() < 15 ||
                    $medidasAereaOd->getMedidasAerea1000() - $medidasOseaOd->getMedidasOsea1000() < 15 ||
                    $medidasAereaOd->getMedidasAerea2000() - $medidasOseaOd->getMedidasOsea2000() < 15 ||
                    $medidasAereaOd->getMedidasAerea3000() - $medidasOseaOd->getMedidasOsea3000() < 15 ||
                    $medidasAereaOd->getMedidasAerea4000() - $medidasOseaOd->getMedidasOsea4000() < 15 ) {

                    $tipoPerdidaOd = 'SENSORIAL';
                }
            }
        }

        /**
        * Existe un diagnóstico audiológico de HA‐Mx cuando:
        **/
        
        //La audición por VO esta alterada (bajo los 20dB).
        if ($medidasOseaOd->getMedidasOsea250() > 20 ||
            $medidasOseaOd->getMedidasOsea500() > 20 ||
            $medidasOseaOd->getMedidasOsea1000() > 20 ||
            $medidasOseaOd->getMedidasOsea2000() > 20 ||
            $medidasOseaOd->getMedidasOsea3000() > 20 ||
            $medidasOseaOd->getMedidasOsea4000() > 20) {

            //La audición por VA esta alterada (bajo los 20dB).
            if ($medidasAereaOd->getMedidasAerea250() > 20 ||
                $medidasAereaOd->getMedidasAerea500() > 20 ||
                $medidasAereaOd->getMedidasAerea1000() > 20 ||
                $medidasAereaOd->getMedidasAerea2000() > 20 ||
                $medidasAereaOd->getMedidasAerea3000() > 20 ||
                $medidasAereaOd->getMedidasAerea4000() > 20) {

                //hay AUSENCIA de GAP ósteo‐aéreo, o éste es superior a 15dB.
                if ($medidasAereaOd->getMedidasAerea250() - $medidasOseaOd->getMedidasOsea250() > 15 ||
                    $medidasAereaOd->getMedidasAerea500() - $medidasOseaOd->getMedidasOsea500() > 15 ||
                    $medidasAereaOd->getMedidasAerea1000() - $medidasOseaOd->getMedidasOsea1000() > 15 ||
                    $medidasAereaOd->getMedidasAerea2000() - $medidasOseaOd->getMedidasOsea2000() > 15 ||
                    $medidasAereaOd->getMedidasAerea3000() - $medidasOseaOd->getMedidasOsea3000() > 15 ||
                    $medidasAereaOd->getMedidasAerea4000() - $medidasOseaOd->getMedidasOsea4000() > 15 ) {

                    $tipoPerdidaOd = 'MIXTA';
                }
            }
        }

        /**
        *  Fin Analizis del Oido Derecho
        **/



        return $this->render('audiometria/show.html.twig', array(
            'audiometrium' => $audiometrium,
            'medidasAereaOi' => $medidasAereaOi,
            'medidasAereaOd' => $medidasAereaOd,
            'medidasOseaOi' => $medidasOseaOi,
            'medidasOseaOd' => $medidasOseaOd,
            'tipoPerdidaOi' => $tipoPerdidaOi,
            'tipoPerdidaOd' => $tipoPerdidaOd,
            'observaciones' => $observaciones,
            'delete_form' => $deleteForm->createView(),
            'persona' => $persona,
        ));

    }

    /**
     * Displays a form to edit an existing Audiometria entity.
     *
     * @Route("/{idAudiometria}/edit", name="audiometria_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Audiometria $audiometrium)
    {
        $persona_id = $request->get('id');

        $persona = $this->getDoctrine()
            ->getRepository('AppBundle:Persona')
            ->find($persona_id);

        $audiometrias = $persona->getAudiometrias();

        if ($audiometrias->isEmpty()) {
            $nombreAudiometria = 'Base';
        }else{

            //obtengo la ultima audiometria
            $ultimaAudiometria = $audiometrias->last();

            if ($ultimaAudiometria->getAudiometriaNombre() == 'Base') {
               $nombreAudiometria = 'Seguimiento';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'NORMAL' && $ultimaAudiometria->getAudiometriaNombre() == 'Seguimiento') {
               $nombreAudiometria = 'Seguimiento';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'ALTERADA' && $ultimaAudiometria->getAudiometriaNombre() == 'Seguimiento') {
               $nombreAudiometria = 'Comprobacion';
            }

            if ($ultimaAudiometria->getAudiometriaEstado() == 'ALTERADA' && $ultimaAudiometria->getAudiometriaNombre() == 'Comprobacion') {
               $nombreAudiometria = 'Confirmacion';
            }
            
        }

        return $this->render('audiometria/edit.html.twig', array(
            'persona' => $persona_id,
            'nombreAudiometria' => $nombreAudiometria,
            )
        );
    }

    /**
     * Deletes a Audiometria entity.
     *
     * @Route("/{id}", name="audiometria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Audiometria $audiometrium)
    {
        $form = $this->createDeleteForm($audiometrium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($audiometrium);
            $em->flush();
        }

        return $this->redirectToRoute('audiometria_index');
    }

    /**
     * Creates a form to delete a Audiometria entity.
     *
     * @param Audiometria $audiometrium The Audiometria entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Audiometria $audiometrium)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('audiometria_delete', array('id' => $audiometrium->getAudiometriaId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     *
     *insercion de los datos de las medidas de la audiometria
     *
     */
    public function agregarMedidasAudiometria($audiometriaId, $request)
    {

        $audiometria = $this->getDoctrine()
            ->getRepository('AppBundle:Audiometria')
            ->find($audiometriaId);

        /**
        *insercion de los datos del oido izquierdo
        */
        $medidasAerea = new MedidasAerea();
        $medidasAerea->setMedidasAereaOido('IZQUIERDO');
        $medidasAerea->setMedidasAerea250($request->get('medidasAerea250_oi'));
        $medidasAerea->setMedidasAerea500($request->get('medidasAerea500_oi'));
        $medidasAerea->setMedidasAerea1000($request->get('medidasAerea1000_oi'));
        $medidasAerea->setMedidasAerea2000($request->get('medidasAerea2000_oi'));
        $medidasAerea->setMedidasAerea3000($request->get('medidasAerea3000_oi'));
        $medidasAerea->setMedidasAerea4000($request->get('medidasAerea4000_oi'));
        $medidasAerea->setMedidasAerea6000($request->get('medidasAerea6000_oi'));
        $medidasAerea->setMedidasAerea8000($request->get('medidasAerea8000_oi'));
        $medidasAerea->setAudiometria($audiometria);
        $medidasAerea->setPromedioPerdida();
        $medidasAerea->setDiagnostico();

        $medidasOsea = new MedidasOsea();
        $medidasOsea->setMedidasOseaOido('IZQUIERDO');
        $medidasOsea->setMedidasOsea250($request->get('medidasOsea250_oi'));
        $medidasOsea->setMedidasOsea500($request->get('medidasOsea500_oi'));
        $medidasOsea->setMedidasOsea1000($request->get('medidasOsea1000_oi'));
        $medidasOsea->setMedidasOsea2000($request->get('medidasOsea2000_oi'));
        $medidasOsea->setMedidasOsea3000($request->get('medidasOsea3000_oi'));
        $medidasOsea->setMedidasOsea4000($request->get('medidasOsea4000_oi'));
        $medidasOsea->setAudiometria($audiometria);


        /**
        *insercion de los datos del oido derecho
        */
        $medidasAerea2 = new MedidasAerea();
        $medidasAerea2->setMedidasAereaOido('DERECHO');
        $medidasAerea2->setMedidasAerea250($request->get('medidasAerea250_od'));
        $medidasAerea2->setMedidasAerea500($request->get('medidasAerea500_od'));
        $medidasAerea2->setMedidasAerea1000($request->get('medidasAerea1000_od'));
        $medidasAerea2->setMedidasAerea2000($request->get('medidasAerea2000_od'));
        $medidasAerea2->setMedidasAerea3000($request->get('medidasAerea3000_od'));
        $medidasAerea2->setMedidasAerea4000($request->get('medidasAerea4000_od'));
        $medidasAerea2->setMedidasAerea6000($request->get('medidasAerea6000_od'));
        $medidasAerea2->setMedidasAerea8000($request->get('medidasAerea8000_od'));
        $medidasAerea2->setAudiometria($audiometria);
        $medidasAerea2->setPromedioPerdida();
        $medidasAerea2->setDiagnostico();

        $medidasOsea2 = new MedidasOsea();
        $medidasOsea2->setMedidasOseaOido('DERECHO');
        $medidasOsea2->setMedidasOsea250($request->get('medidasOsea250_od'));
        $medidasOsea2->setMedidasOsea500($request->get('medidasOsea500_od'));
        $medidasOsea2->setMedidasOsea1000($request->get('medidasOsea1000_od'));
        $medidasOsea2->setMedidasOsea2000($request->get('medidasOsea2000_od'));
        $medidasOsea2->setMedidasOsea3000($request->get('medidasOsea3000_od'));
        $medidasOsea2->setMedidasOsea4000($request->get('medidasOsea4000_od'));
        $medidasOsea2->setAudiometria($audiometria);


        $audiometria->addMedidasAerea($medidasAerea);
        $audiometria->addMedidasAerea($medidasAerea2);

        $audiometria->addMedidasOsea($medidasOsea);
        $audiometria->addMedidasOsea($medidasOsea2);

        $em = $this->getDoctrine()->getManager();
        /*
        $em->persist($medidasAerea);
        $em->flush();
        $em->persist($medidasOsea);
        $em->flush();
        $em->persist($medidasAerea2);
        $em->flush();
        $em->persist($medidasOsea2);
        $em->flush();
        */
        return true;


    }
        
}
