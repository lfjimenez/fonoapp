<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Area;
use AppBundle\Form\AreaType;

/**
 * Area controller.
 *
 * @Route("/alertas")
 */
class AlertasController extends Controller
{


    /**
     * Finds today calendar events .
     *
     * @Route("/", name="alerta_index")
     * @Method("GET")
     */
    public function indexAction(Request $request){

        //$request = $this->getMasterRequest();

        $googleCalendar = $this->get('fungio.google_calendar');
        $redirectUri = 'http://localhost:8000/alertas';
        $googleCalendar->setRedirectUri($redirectUri);

        if ($request->query->has('code') && $request->get('code')) {
            $client = $googleCalendar->getClient($request->get('code'));
        } else {
            $client = $googleCalendar->getClient();
        }

        if (is_string($client)) {
            return new RedirectResponse($client);
        }

        $events = $googleCalendar->getEventsForDate('primary', new \DateTime('now'));

        return $this->render('alertas/show.html.twig', array(
            'eventos' => $events,
        ));
    }

    /**
     * Creates a new Event
     *
     * @Route("/new", name="event_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $googleCalendar = $this->get('fungio.google_calendar');
        $redirectUri = 'http://localhost:8000/alertas';
        $googleCalendar->setRedirectUri($redirectUri);

        if ($request->query->has('code') && $request->get('code')) {
            $client = $googleCalendar->getClient($request->get('code'));
        } else {
            $client = $googleCalendar->getClient();
        }

        if (is_string($client)) {
            return new RedirectResponse($client);
        }

        $fechaActual = new \DateTime('now');
        $fechaActual->format('Y-m-d H:i:s');
        
        if ($request->get('exposicionRuido') == 10) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria de seguimiento al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 5 años';
            $fechaFutura = $fechaActual->add(new \DateInterval('P5Y'));
        }elseif ($request->get('exposicionRuido') == 11) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria de seguimiento al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 1 año';
            $fechaFutura = $fechaActual->add(new \DateInterval('P12M'));
        }elseif ($request->get('exposicionRuido') == 12) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria de seguimiento al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 6 meses';
            $fechaFutura = $fechaActual->add(new \DateInterval('P6M'));
        }

        $googleCalendar->addEvent('primary', 
            new \DateTime($fechaActual->format('Y-m-d H:i:s')),
            new \DateTime($fechaFutura->format('Y-m-d H:i:s')),
            $eventSummary,
            $eventDescription,
            $eventAttendee = "",
            $location = "",
            $optionalParams = [],
            $allDay = false);

        return $this->redirectToRoute('persona_index');
    }

    /**
     * Creates a new Event
     *
     * @Route("/new_2", name="event_new2")
     * @Method({"GET", "POST"})
     */
    public function new2Action(Request $request)
    {
        $googleCalendar = $this->get('fungio.google_calendar');
        $redirectUri = 'http://localhost:8000/alertas';
        $googleCalendar->setRedirectUri($redirectUri);

        if ($request->query->has('code') && $request->get('code')) {
            $client = $googleCalendar->getClient($request->get('code'));
        } else {
            $client = $googleCalendar->getClient();
        }

        if (is_string($client)) {
            return new RedirectResponse($client);
        }

        $fechaActual = new \DateTime('now');
        $fechaActual->format('Y-m-d H:i:s');
        
        if ($request->get('exposicionRuido') == 10) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 5 años';
            $fechaFutura = $fechaActual->add(new DateInterval('P1D'));
        }elseif ($request->get('exposicionRuido') == 11) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 1 año';
            $fechaFutura = $fechaActual->add(new \DateInterval('P1D'));
        }elseif ($request->get('exposicionRuido') == 12) {
            $eventDescription = 'Realizar audiometria a paciente: ' . $request->get('cedula') ;
            $eventSummary = 'Se debe hacer una audiometria al paciente con cedula: '. $request->get('cedula') . ', la ultima audiometria realizada fue hace 6 meses';
            $fechaFutura = $fechaActual->add(new \DateInterval('P1D'));
        }

        $googleCalendar->addEvent('primary', 
            new \DateTime($fechaActual->format('Y-m-d H:i:s')),
            new \DateTime($fechaFutura->format('Y-m-d H:i:s')),
            $eventSummary,
            $eventDescription,
            $eventAttendee = "",
            $location = "",
            $optionalParams = [],
            $allDay = false);

        return $this->redirectToRoute('persona_index');
    }
}
