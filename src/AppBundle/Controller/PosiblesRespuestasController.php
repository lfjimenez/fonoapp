<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\PosiblesRespuestas;
use AppBundle\Form\PosiblesRespuestasType;

/**
 * PosiblesRespuestas controller.
 *
 * @Route("/posiblesrespuestas")
 */
class PosiblesRespuestasController extends Controller
{
    /**
     * Lists all PosiblesRespuestas entities.
     *
     * @Route("/", name="posiblesrespuestas_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posiblesRespuestas = $em->getRepository('AppBundle:PosiblesRespuestas')->findAll();

        return $this->render('posiblesrespuestas/index.html.twig', array(
            'posiblesRespuestas' => $posiblesRespuestas,
        ));
    }

    /**
     * Creates a new PosiblesRespuestas entity.
     *
     * @Route("/new", name="posiblesrespuestas_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $posiblesRespuesta = new PosiblesRespuestas();
        $form = $this->createForm('AppBundle\Form\PosiblesRespuestasType', $posiblesRespuesta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($posiblesRespuesta);
            $em->flush();

            return $this->redirectToRoute('posiblesrespuestas_show', array('id' => $posiblesRespuesta->getId()));
        }

        return $this->render('posiblesrespuestas/new.html.twig', array(
            'posiblesRespuesta' => $posiblesRespuesta,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PosiblesRespuestas entity.
     *
     * @Route("/{id}", name="posiblesrespuestas_show")
     * @Method("GET")
     */
    public function showAction(PosiblesRespuestas $posiblesRespuesta)
    {
        $deleteForm = $this->createDeleteForm($posiblesRespuesta);

        return $this->render('posiblesrespuestas/show.html.twig', array(
            'posiblesRespuesta' => $posiblesRespuesta,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PosiblesRespuestas entity.
     *
     * @Route("/{id}/edit", name="posiblesrespuestas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PosiblesRespuestas $posiblesRespuesta)
    {
        $deleteForm = $this->createDeleteForm($posiblesRespuesta);
        $editForm = $this->createForm('AppBundle\Form\PosiblesRespuestasType', $posiblesRespuesta);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($posiblesRespuesta);
            $em->flush();

            return $this->redirectToRoute('posiblesrespuestas_edit', array('id' => $posiblesRespuesta->getId()));
        }

        return $this->render('posiblesrespuestas/edit.html.twig', array(
            'posiblesRespuesta' => $posiblesRespuesta,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PosiblesRespuestas entity.
     *
     * @Route("/{id}", name="posiblesrespuestas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PosiblesRespuestas $posiblesRespuesta)
    {
        $form = $this->createDeleteForm($posiblesRespuesta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($posiblesRespuesta);
            $em->flush();
        }

        return $this->redirectToRoute('posiblesrespuestas_index');
    }

    /**
     * Creates a form to delete a PosiblesRespuestas entity.
     *
     * @param PosiblesRespuestas $posiblesRespuesta The PosiblesRespuestas entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PosiblesRespuestas $posiblesRespuesta)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('posiblesrespuestas_delete', array('id' => $posiblesRespuesta->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
