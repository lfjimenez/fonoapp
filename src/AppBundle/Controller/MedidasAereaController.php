<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\MedidasAerea;
use AppBundle\Form\MedidasAereaType;

/**
 * MedidasAerea controller.
 *
 * @Route("/medidasaerea")
 */
class MedidasAereaController extends Controller
{
    /**
     * Lists all MedidasAerea entities.
     *
     * @Route("/", name="medidasaerea_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $medidasAereas = $em->getRepository('AppBundle:MedidasAerea')->findAll();

        return $this->render('medidasaerea/index.html.twig', array(
            'medidasAereas' => $medidasAereas,
        ));
    }

    /**
     * Creates a new MedidasAerea entity.
     *
     * @Route("/new", name="medidasaerea_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $medidasAerea = new MedidasAerea();
        $form = $this->createForm('AppBundle\Form\MedidasAereaType', $medidasAerea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medidasAerea);
            $em->flush();

            return $this->redirectToRoute('medidasaerea_show', array('id' => $medidasAerea->getId()));
        }

        return $this->render('medidasaerea/new.html.twig', array(
            'medidasAerea' => $medidasAerea,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MedidasAerea entity.
     *
     * @Route("/{id}", name="medidasaerea_show")
     * @Method("GET")
     */
    public function showAction(MedidasAerea $medidasAerea)
    {
        $deleteForm = $this->createDeleteForm($medidasAerea);

        return $this->render('medidasaerea/show.html.twig', array(
            'medidasAerea' => $medidasAerea,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MedidasAerea entity.
     *
     * @Route("/{id}/edit", name="medidasaerea_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MedidasAerea $medidasAerea)
    {
        $deleteForm = $this->createDeleteForm($medidasAerea);
        $editForm = $this->createForm('AppBundle\Form\MedidasAereaType', $medidasAerea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medidasAerea);
            $em->flush();

            return $this->redirectToRoute('medidasaerea_edit', array('id' => $medidasAerea->getId()));
        }

        return $this->render('medidasaerea/edit.html.twig', array(
            'medidasAerea' => $medidasAerea,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a MedidasAerea entity.
     *
     * @Route("/{id}", name="medidasaerea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MedidasAerea $medidasAerea)
    {
        $form = $this->createDeleteForm($medidasAerea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($medidasAerea);
            $em->flush();
        }

        return $this->redirectToRoute('medidasaerea_index');
    }

    /**
     * Creates a form to delete a MedidasAerea entity.
     *
     * @param MedidasAerea $medidasAerea The MedidasAerea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MedidasAerea $medidasAerea)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('medidasaerea_delete', array('id' => $medidasAerea->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
