<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\MedidasOsea;
use AppBundle\Form\MedidasOseaType;

/**
 * MedidasOsea controller.
 *
 * @Route("/medidasosea")
 */
class MedidasOseaController extends Controller
{
    /**
     * Lists all MedidasOsea entities.
     *
     * @Route("/", name="medidasosea_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $medidasOseas = $em->getRepository('AppBundle:MedidasOsea')->findAll();

        return $this->render('medidasosea/index.html.twig', array(
            'medidasOseas' => $medidasOseas,
        ));
    }

    /**
     * Creates a new MedidasOsea entity.
     *
     * @Route("/new", name="medidasosea_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $medidasOsea = new MedidasOsea();
        $form = $this->createForm('AppBundle\Form\MedidasOseaType', $medidasOsea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medidasOsea);
            $em->flush();

            return $this->redirectToRoute('medidasosea_show', array('id' => $medidasOsea->getId()));
        }

        return $this->render('medidasosea/new.html.twig', array(
            'medidasOsea' => $medidasOsea,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MedidasOsea entity.
     *
     * @Route("/{id}", name="medidasosea_show")
     * @Method("GET")
     */
    public function showAction(MedidasOsea $medidasOsea)
    {
        $deleteForm = $this->createDeleteForm($medidasOsea);

        return $this->render('medidasosea/show.html.twig', array(
            'medidasOsea' => $medidasOsea,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MedidasOsea entity.
     *
     * @Route("/{id}/edit", name="medidasosea_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MedidasOsea $medidasOsea)
    {
        $deleteForm = $this->createDeleteForm($medidasOsea);
        $editForm = $this->createForm('AppBundle\Form\MedidasOseaType', $medidasOsea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($medidasOsea);
            $em->flush();

            return $this->redirectToRoute('medidasosea_edit', array('id' => $medidasOsea->getId()));
        }

        return $this->render('medidasosea/edit.html.twig', array(
            'medidasOsea' => $medidasOsea,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a MedidasOsea entity.
     *
     * @Route("/{id}", name="medidasosea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MedidasOsea $medidasOsea)
    {
        $form = $this->createDeleteForm($medidasOsea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($medidasOsea);
            $em->flush();
        }

        return $this->redirectToRoute('medidasosea_index');
    }

    /**
     * Creates a form to delete a MedidasOsea entity.
     *
     * @param MedidasOsea $medidasOsea The MedidasOsea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MedidasOsea $medidasOsea)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('medidasosea_delete', array('id' => $medidasOsea->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
