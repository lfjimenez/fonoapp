<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Encuesta;
use AppBundle\Entity\Pregunta;
use AppBundle\Entity\Respuesta;
use AppBundle\Entity\Historia;
use AppBundle\Entity\PersonaRespuesta;
use AppBundle\Form\PersonaType;

/**
 * Historial controller.
 *
 * @Route("/historia")
 */
class HistoriaController extends Controller
{
    /**
     * Lists all Historias from one person
     *
     * @Route("/", name="historia_show")
     * @Method("GET")
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $historia = $em->getRepository('AppBundle:Historia')->find($_GET['idHistoria']);

        $persona = $em->getRepository('AppBundle:Persona')->findOneBypersonaId($historia->getPersona());

        //fecha actual
        $dia=date('d');
        $mes=date('m');
        $ano=date('Y');

        //fecha de nacimiento
        $fecnac = $persona->getPersonaFecnac();

        $dianaz=$fecnac->format('d');
        $mesnaz=$fecnac->format('m');
        $anonaz=$fecnac->format('Y');

        //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
        if (($mesnaz == $mes) && ($dianaz > $dia)) {
        $ano=($ano-1); }

        //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
        if ($mesnaz > $mes) {
        $ano=($ano-1);}

        //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
        $edad=($ano-$anonaz);
        //print $edad;

        if (isset($persona)) {
            $historias = $historia->getPersonaRespuesta();
        }else{
            $historias = null;
        }
        return $this->render('formulario/resumen_historia.html.twig', array(
            'historias' => $historias,
            'persona' => $persona,
            'edad' => $edad
        ));
    }

    /**
     * create a new historia
     *
     * @Route("/new", name="historia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction()
    {

        $em = $this->getDoctrine()->getManager();
        $persona = $em->getRepository('AppBundle:Persona')->findOneBypersonaId($_GET['idPersona']);

        $encuestas = $em->getRepository('AppBundle:Encuesta')->findAll();
        $preguntas = $em->getRepository('AppBundle:Pregunta')->findAll();
        $posiblesRespuestas = $em->getRepository('AppBundle:PosiblesRespuestas')->findAll();

        return $this->render('formulario/new.html.twig', array(
            'persona'=> $persona, 
            'encuestas' => $encuestas,
            'preguntas' => $preguntas,
            'posiblesRespuestas' => $posiblesRespuestas
        ));
    }

    /**
     * save an historia in db
     *
     * @Route("/save", name="historia_save")
     * @Method({"GET", "POST"})
     */
    public function saveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $personaId = $_POST['personaId'];
        $persona = $em->getRepository('AppBundle:Persona')->find($personaId);

        $historia = new Historia();
        $historia->setPersona($persona);
        $em->persist($historia);
        $em->flush();

        $ultimaHistoriaId = $historia->getHistoriaId();

        $preguntas = $request->request;
        $counter = 0;

        foreach ($preguntas as $pregunta => $value) {

            if ($counter == 0) {
                $counter = $counter + 1;
                continue;
            }
            $preguntaId = substr($pregunta, 9);
            $preguntaObj = $em->getRepository('AppBundle:Pregunta')->find($preguntaId);

            $posibleRespuestaId = $value;
            $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find($posibleRespuestaId);
            
            //guardo la exposiccion a ruido
            if ($preguntaId  == '19') {
                $exposicionRuido = $posibleRespuestaId;
            }

            $respuesta = new Respuesta();
            $respuesta->setPersonaId($persona);
            $respuesta->setPreguntaId($preguntaObj);
            $respuesta->setPosibleRespuesta($posibleRespuestaObj);
            if ($posibleRespuestaObj == null) {
                $respuesta->setRespuestaDescripcion($value);
            }

            $em->persist($respuesta);

            $personaRespuesta = new PersonaRespuesta();
            $personaRespuesta->setRespuesta($respuesta);
            $personaRespuesta->setHistoria($historia);

            $em->persist($personaRespuesta);
            
        }
        
        $em->flush();

        //genero la alerta dependiendo del valor de "expuesto a ruido"
        if ($exposicionRuido == 10 || $exposicionRuido == 11 || $exposicionRuido == 12) {
            $cedula = $persona->getPersonaCedula();
            return $this->redirectToRoute('event_new',array('exposicionRuido' => $exposicionRuido, 'cedula' => $cedula));
        }

        return $this->redirectToRoute('persona_index');

    }

    /**
     * Displays a form to edit an existing Persona entity.
     *
     * @Route("/{id}/edit", name="persona_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Persona $persona)
    {
        $deleteForm = $this->createDeleteForm($persona);
        $editForm = $this->createForm('AppBundle\Form\PersonaType', $persona);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($persona);
            $em->flush();

            return $this->redirectToRoute('persona_edit', array('id' => $persona->getPersonaId()));
        }

        return $this->render('persona/edit.html.twig', array(
            'persona' => $persona,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Persona entity.
     *
     * @Route("/{id}", name="persona_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Persona $persona)
    {
        $form = $this->createDeleteForm($persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($persona);
            $em->flush();
        }

        return $this->redirectToRoute('persona_index');
    }

    /**
     * Creates a form to delete a Persona entity.
     *
     * @param Persona $persona The Persona entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Persona $persona)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('persona_delete', array('id' => $persona->getPersonaId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
