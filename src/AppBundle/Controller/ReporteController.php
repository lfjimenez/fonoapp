<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Encuesta;
use AppBundle\Entity\Pregunta;
use AppBundle\Entity\Respuesta;
use AppBundle\Form\PersonaType;

/**
 * Reporte controller.
 *
 * @Route("/reporte")
 */
class ReporteController extends Controller
{
    /**
     * Lists reporte index
     *
     * @Route("/", name="reporte_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $areas = $em->getRepository('AppBundle:Area')->findAll();

        return $this->render('reporte/index.html.twig', array(
            'areas' => $areas,
        ));
    }

    /**
     * Lists all persons from an area
     *
     * @Route("/reporte_area", name="reporte_area")
     * @Method("GET")
     */
    public function showAreaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $area = $em->getRepository('AppBundle:Area')->find($_GET['idArea']);

        $personas = $area->getPersonas();

        return $this->render('reporte/show.html.twig', array(
            'personas' => $personas,
        ));
    }


}
