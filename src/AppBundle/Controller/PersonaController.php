<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Persona;
use AppBundle\Entity\Area;
use AppBundle\Entity\Audiometria;
use AppBundle\Entity\Pregunta;
use AppBundle\Entity\Respuesta;
use AppBundle\Entity\PersonaRespuesta;
use AppBundle\Entity\MedidasAerea;
use AppBundle\Entity\MedidasOsea;
use AppBundle\Form\PersonaType;
/**
 * Persona controller.
 *
 * @Route("/persona")
 */
class PersonaController extends Controller
{
    /**
     * Lists all Persona entities.
     *
     * @Route("/", name="persona_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        //dump($this->getUser());

        if (in_array('ROLE_SUPER_ADMIN',$user->getRoles())){
            $personas = $em->getRepository('AppBundle:Persona')->findAll();
        }elseif (in_array('ROLE_ADMIN',$user->getRoles())) {
            $empresa = $user->getPersona()->getEmpresa();
            $personas = $em->getRepository('AppBundle:Persona')->findByEmpresa($empresa);
        }

        return $this->render('persona/index.html.twig', array(
            'personas' => $personas,
        ));
    }

    /**
     * Creates a new Persona entity.
     *
     * @Route("/new", name="persona_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $empresa = $user->getPersona()->getEmpresa();
        $persona = new Persona();
        $persona->setEmpresa($empresa);
        $form = $this->createForm('AppBundle\Form\PersonaType', $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($persona);
            $em->flush();

            return $this->redirectToRoute('persona_show', array('id' => $persona->getPersonaId()));
        }

        return $this->render('persona/new.html.twig', array(
            'persona' => $persona,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Persona entity.
     *
     * @Route("/{id}", name="persona_show")
     * @Method("GET")
     */
    public function showAction(Persona $persona)
    {
        $deleteForm = $this->createDeleteForm($persona);

        $audiometrias = $persona->getAudiometrias();
        $historias = $persona->getHistorias();

        return $this->render('persona/show.html.twig', array(
            'persona' => $persona,
            'audiometrias' => $audiometrias,
            'historias' => $historias,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Persona entity.
     *
     * @Route("/{id}/edit", name="persona_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Persona $persona)
    {
        $deleteForm = $this->createDeleteForm($persona);
        $editForm = $this->createForm('AppBundle\Form\PersonaType', $persona);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($persona);
            $em->flush();

            return $this->redirectToRoute('persona_edit', array('id' => $persona->getPersonaId()));
        }

        return $this->render('persona/edit.html.twig', array(
            'persona' => $persona,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Persona entity.
     *
     * @Route("/{id}", name="persona_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Persona $persona)
    {
        $form = $this->createDeleteForm($persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($persona);
            $em->flush();
        }

        return $this->redirectToRoute('persona_index');
    }

    /**
     * Creates a form to delete a Persona entity.
     *
     * @param Persona $persona The Persona entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Persona $persona)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('persona_delete', array('id' => $persona->getPersonaId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Finds and displays a Persona entity.
     *
     * @Route("/find", name="persona_find")
     * @Method("POST")
     */
    public function findAction(Request $request)
    {
        $cedula = $request->request->get('cedula_persona');

        $em = $this->getDoctrine()->getManager();
        $personas = $em->getRepository('AppBundle:Persona')->findBypersonaCedula($cedula);

        dump($personas);

        return $this->render('persona/index.html.twig', array(
            'personas' => $personas,
        ));
    }

    /**
     * load personas from excel file
     *
     * @Route("/load_personas", name="load_personas")
     * @Method({"GET", "POST"})
     */
    public function loadPersonasAction(Request $request)
    {
        
        $fileName = $request->request->get('fileName');
        $fileLocation = $this->get('kernel')->getRootDir() . '/../web/uploads/archivos_excel/'.$fileName;

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($fileLocation);

        $em = $this->getDoctrine()->getManager();

        $i = 0;
        while ($phpExcelObject->setActiveSheetIndex($i)){

            $objWorksheet = $phpExcelObject->getActiveSheet();

            //datos personales
            $nombres = $objWorksheet->getCell('B3')->getValue();
            $apellidos = $objWorksheet->getCell('B4')->getValue();
            $sexo = $objWorksheet->getCell('B5')->getValue();
            $fecnac = $objWorksheet->getCell('B6')->getValue();
            $cedula = $objWorksheet->getCell('B7')->getValue();
            $direccion = $objWorksheet->getCell('B8')->getValue();
            $barrio = $objWorksheet->getCell('B9')->getValue();
            $telefono = $objWorksheet->getCell('B10')->getValue();

            $persona = new Persona();
            $persona->setPersonaNombres($nombres);
            $persona->setPersonaApellidos($apellidos);
            $persona->setPersonaSexo($sexo);
            $persona->setPersonaFecnac(new \DateTime($fecnac));
            $persona->setPersonaCedula($cedula);
            $persona->setPersonaDireccion($direccion);
            $persona->setPersonaBarrio($barrio);
            $persona->setPersonaTelefono($telefono);

            $empresa = $em->getRepository('AppBundle:Empresa')->find(1);
            $persona->setEmpresa($empresa);

            $areaNombre = $objWorksheet->getCell('B29')->getValue();
            $area = new Area();
            $area = $em->getRepository('AppBundle:Area')->findByareaNombre($areaNombre);
            $persona->setArea($area[0]);

            try {
                $em->persist($persona);
                $em->flush();
            } catch (Exception $e) {
                dump($e->getMessage());
            }

            //datos de historia clinica
            $respuestas = array();
            $respuestas[1] = $objWorksheet->getCell('B14')->getValue();  //hipertension
            $respuestas[2] = $objWorksheet->getCell('B15')->getValue();  //hipercolesterolemia
            $respuestas[3] = $objWorksheet->getCell('B16')->getValue();  //hipotiroidismo
            $respuestas[4] = $objWorksheet->getCell('B17')->getValue();  //Barotrauma
            $respuestas[5] = $objWorksheet->getCell('B20')->getValue();  //diabetes mellitus
            $respuestas[6] = $objWorksheet->getCell('B21')->getValue();  //Enfermedad renal
            $respuestas[7] = $objWorksheet->getCell('B18')->getValue();  //traumatismo acustico agudo
            $respuestas[8] = $objWorksheet->getCell('B19')->getValue();  //vibraciones

            $respuestas[9] = $objWorksheet->getCell('B41')->getValue();  //elementos de proteccion auditiva?
            $respuestas[10] = $objWorksheet->getCell('B34')->getValue();  //exposicion a ruido

            $respuestas[11] = $objWorksheet->getCell('B55')->getValue();  //discoteca
            $respuestas[12] = $objWorksheet->getCell('B58')->getValue();  //casa
            $respuestas[13] = $objWorksheet->getCell('B59')->getValue();  //moto-medio de transporte

            $respuestas[14] = $objWorksheet->getCell('B47')->getValue();  //aculenos o tinnitus
            $respuestas[15] = $objWorksheet->getCell('B50')->getValue();  //vertigo

            $respuestas[16] = $objWorksheet->getCell('B22')->getValue();  //fuma
            $respuestas[17] = $objWorksheet->getCell('B23')->getValue();  //bebe

            $respuestas[18] = $objWorksheet->getCell('B28')->getValue();  //motivo de evaluacion
            $respuestas[19] = $objWorksheet->getCell('B36')->getValue();  //tiempo expuesto a ruido
            $respuestas[20] = 'sin observaciones';  //observaciones no esta en en excel
            
            foreach ($respuestas as $key => $value) {
                $pregunta = $em->getRepository('AppBundle:Pregunta')->find($key);
                if ($value == 'SI') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(1);
                }elseif ($value == 'NO') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(2);
                }elseif ($value == 'BASE') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(3);
                }elseif ($value == 'SEGUIMIENTO') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(4);
                }elseif ($value == 'CONFIRMACION') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(5);
                }elseif ($value == 'Ninguno') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(6);
                }elseif ($value == 'Menor a 8') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(7);
                }elseif ($value == 'Igual a 8') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(8);
                }elseif ($value == 'Mayor a 8') {
                    $posibleRespuestaObj = $em->getRepository('AppBundle:PosiblesRespuestas')->find(9);
                }else{
                    $posibleRespuestaObj = null;
                }
                
                $respuestaObj = new Respuesta();
                $respuestaObj->setPersonaId($persona);
                $respuestaObj->setPreguntaId($pregunta);
                $respuestaObj->setPosibleRespuesta($posibleRespuestaObj);
                if ($posibleRespuestaObj == null) {
                    $respuestaObj->setRespuestaDescripcion($value);
                }

                $em->persist($respuestaObj);

                $personaRespuesta = new PersonaRespuesta();
                $personaRespuesta->setRespuesta($respuestaObj);
                $personaRespuesta->setPersona($persona);

                $em->persist($personaRespuesta);
                
            }
            $em->flush();

            /**
            * insercion de los datos de las audimetrias
            */
            
            if ($objWorksheet->getCell('B95')->getValue() != null ) {

                $audiometria = new Audiometria();
                
                $InvDate = $objWorksheet->getCell('B93')->getFormattedValue();
                //$date = new \DateTime($InvDate);
                $date = \DateTime::createFromFormat('Y-m-d H:i:s', $InvDate);
                $audiometria->setAudiometriaFecha($date);

                $audiometria->setPersona($persona);

                $em->persist($audiometria);
                $em->flush(); 
                echo "<pre>";
                print_r($audiometria);
                echo "</pre>";
                exit();

                /**
                *insercion de los datos del oido izquierdo
                */
                $medidasAerea = new MedidasAerea();
                $medidasAerea->setMedidasAereaOido('IZQUIERDO');
                $medidasAerea->setMedidasAerea250($objWorksheet->getCell('B113')->getValue());
                $medidasAerea->setMedidasAerea500($objWorksheet->getCell('B114')->getValue());
                $medidasAerea->setMedidasAerea1000($objWorksheet->getCell('B115')->getValue());
                $medidasAerea->setMedidasAerea2000($objWorksheet->getCell('B116')->getValue());
                $medidasAerea->setMedidasAerea3000($objWorksheet->getCell('B117')->getValue());
                $medidasAerea->setMedidasAerea4000($objWorksheet->getCell('B118')->getValue());
                $medidasAerea->setMedidasAerea6000($objWorksheet->getCell('B119')->getValue());
                $medidasAerea->setMedidasAerea8000($objWorksheet->getCell('B120')->getValue());
                $medidasAerea->setAudiometria($audiometria);
                $medidasAerea->setPromedioPerdida();

                $medidasAerea->setDiagnostico();

                $medidasOsea = new MedidasOsea();
                $medidasOsea->setMedidasOseaOido('IZQUIERDO');
                $medidasOsea->setMedidasOsea250($objWorksheet->getCell('B123')->getValue());
                $medidasOsea->setMedidasOsea500($objWorksheet->getCell('B124')->getValue());
                $medidasOsea->setMedidasOsea1000($objWorksheet->getCell('B125')->getValue());
                $medidasOsea->setMedidasOsea2000($objWorksheet->getCell('B126')->getValue());
                $medidasOsea->setMedidasOsea3000($objWorksheet->getCell('B127')->getValue());
                $medidasOsea->setMedidasOsea4000($objWorksheet->getCell('B128')->getValue());
                $medidasOsea->setAudiometria($audiometria);


                /**
                *insercion de los datos del oido derecho
                */
                $medidasAerea2 = new MedidasAerea();
                $medidasAerea2->setMedidasAereaOido('DERECHO');
                $medidasAerea2->setMedidasAerea250($objWorksheet->getCell('B95')->getValue());
                $medidasAerea2->setMedidasAerea500($objWorksheet->getCell('B96')->getValue());
                $medidasAerea2->setMedidasAerea1000($objWorksheet->getCell('B97')->getValue());
                $medidasAerea2->setMedidasAerea2000($objWorksheet->getCell('B98')->getValue());
                $medidasAerea2->setMedidasAerea3000($objWorksheet->getCell('B99')->getValue());
                $medidasAerea2->setMedidasAerea4000($objWorksheet->getCell('B100')->getValue());
                $medidasAerea2->setMedidasAerea6000($objWorksheet->getCell('B101')->getValue());
                $medidasAerea2->setMedidasAerea8000($objWorksheet->getCell('B102')->getValue());
                $medidasAerea2->setAudiometria($audiometria);
                $medidasAerea2->setPromedioPerdida();
                $medidasAerea2->setDiagnostico();

                $medidasOsea2 = new MedidasOsea();
                $medidasOsea2->setMedidasOseaOido('DERECHO');
                $medidasOsea2->setMedidasOsea250($objWorksheet->getCell('B1051')->getValue());
                $medidasOsea2->setMedidasOsea500($objWorksheet->getCell('B106')->getValue());
                $medidasOsea2->setMedidasOsea1000($objWorksheet->getCell('B107')->getValue());
                $medidasOsea2->setMedidasOsea2000($objWorksheet->getCell('B108')->getValue());
                $medidasOsea2->setMedidasOsea3000($objWorksheet->getCell('B109')->getValue());
                $medidasOsea2->setMedidasOsea4000($objWorksheet->getCell('B110')->getValue());
                $medidasOsea2->setAudiometria($audiometria);

                $em = $this->getDoctrine()->getManager();
                /*
                $em->persist($medidasAerea);
                $em->persist($medidasOsea);
                $em->persist($medidasAerea2);
                $em->persist($medidasOsea2);
                */

                $audiometria->addMedidasOsea($medidasOsea);
                $audiometria->addMedidasOsea($medidasOsea2);
                $audiometria->addMedidasAerea($medidasAerea);
                $audiometria->addMedidasAerea($medidasAerea2);

                $em->persist($audiometria);
                $em->flush(); 
            }
            if ($objWorksheet->getCell('C95')->getValue() != null ) {

                $audiometria = new Audiometria();

                $audiometria->setAudiometriaFecha(new \DateTime($objWorksheet->getCell('C93')->getValue()));
                $audiometria->setPersona($persona);
                
                //datos de medidasAereaOd
                $medidasAereaOd = array();
                $medidasAereaOd[1] = $objWorksheet->getCell('C95')->getValue(); 
                $medidasAereaOd[2] = $objWorksheet->getCell('C96')->getValue();  
                $medidasAereaOd[3] = $objWorksheet->getCell('C97')->getValue();  
                $medidasAereaOd[4] = $objWorksheet->getCell('C98')->getValue();  
                $medidasAereaOd[5] = $objWorksheet->getCell('C99')->getValue(); 
                $medidasAereaOd[6] = $objWorksheet->getCell('C100')->getValue();  
                $medidasAereaOd[7] = $objWorksheet->getCell('C101')->getValue();  
                $medidasAereaOd[8] = $objWorksheet->getCell('C102')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('C105')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('C106')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('C107')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('C108')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('C109')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('C110')->getValue();  

                //datos de medidasaereaOi
                $medidasAereaOi = array();
                $medidasAereaOi[1] = $objWorksheet->getCell('C113')->getValue(); 
                $medidasAereaOi[2] = $objWorksheet->getCell('C114')->getValue();  
                $medidasAereaOi[3] = $objWorksheet->getCell('C115')->getValue();  
                $medidasAereaOi[4] = $objWorksheet->getCell('C116')->getValue();  
                $medidasAereaOi[5] = $objWorksheet->getCell('C117')->getValue(); 
                $medidasAereaOi[6] = $objWorksheet->getCell('C118')->getValue();  
                $medidasAereaOi[7] = $objWorksheet->getCell('C119')->getValue();  
                $medidasAereaOi[8] = $objWorksheet->getCell('C120')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('C123')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('C124')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('C125')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('C126')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('C127')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('C128')->getValue();  
            }
            if ($objWorksheet->getCell('D95')->getValue() != null ) {

                $audiometria = new Audiometria();

                $audiometria->setAudiometriaFecha(new \DateTime($objWorksheet->getCell('D93')->getValue()));
                $audiometria->setPersona($persona);
                
                //datos de medidasAereaOd
                $medidasAereaOd = array();
                $medidasAereaOd[1] = $objWorksheet->getCell('D95')->getValue(); 
                $medidasAereaOd[2] = $objWorksheet->getCell('D96')->getValue();  
                $medidasAereaOd[3] = $objWorksheet->getCell('D97')->getValue();  
                $medidasAereaOd[4] = $objWorksheet->getCell('D98')->getValue();  
                $medidasAereaOd[5] = $objWorksheet->getCell('D99')->getValue(); 
                $medidasAereaOd[6] = $objWorksheet->getCell('D100')->getValue();  
                $medidasAereaOd[7] = $objWorksheet->getCell('D101')->getValue();  
                $medidasAereaOd[8] = $objWorksheet->getCell('D102')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('D105')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('D106')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('D107')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('D108')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('D109')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('D110')->getValue();  

                //datos de medidasaereaOi
                $medidasAereaOi = array();
                $medidasAereaOi[1] = $objWorksheet->getCell('D113')->getValue(); 
                $medidasAereaOi[2] = $objWorksheet->getCell('D114')->getValue();  
                $medidasAereaOi[3] = $objWorksheet->getCell('D115')->getValue();  
                $medidasAereaOi[4] = $objWorksheet->getCell('D116')->getValue();  
                $medidasAereaOi[5] = $objWorksheet->getCell('D117')->getValue(); 
                $medidasAereaOi[6] = $objWorksheet->getCell('D118')->getValue();  
                $medidasAereaOi[7] = $objWorksheet->getCell('D119')->getValue();  
                $medidasAereaOi[8] = $objWorksheet->getCell('D120')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('D123')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('D124')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('D125')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('D126')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('D127')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('D128')->getValue();  
            }
            if ($objWorksheet->getCell('E95')->getValue() != null ) {

                $audiometria = new Audiometria();

                $audiometria->setAudiometriaFecha(new \DateTime($objWorksheet->getCell('E93')->getValue()));
                $audiometria->setPersona($persona);
                
                //datos de medidasAereaOd
                $medidasAereaOd = array();
                $medidasAereaOd[1] = $objWorksheet->getCell('E95')->getValue(); 
                $medidasAereaOd[2] = $objWorksheet->getCell('E96')->getValue();  
                $medidasAereaOd[3] = $objWorksheet->getCell('E97')->getValue();  
                $medidasAereaOd[4] = $objWorksheet->getCell('E98')->getValue();  
                $medidasAereaOd[5] = $objWorksheet->getCell('E99')->getValue(); 
                $medidasAereaOd[6] = $objWorksheet->getCell('E100')->getValue();  
                $medidasAereaOd[7] = $objWorksheet->getCell('E101')->getValue();  
                $medidasAereaOd[8] = $objWorksheet->getCell('E102')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('E105')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('E106')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('E107')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('E108')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('E109')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('E110')->getValue();  

                //datos de medidasaereaOi
                $medidasAereaOi = array();
                $medidasAereaOi[1] = $objWorksheet->getCell('E113')->getValue(); 
                $medidasAereaOi[2] = $objWorksheet->getCell('E114')->getValue();  
                $medidasAereaOi[3] = $objWorksheet->getCell('E115')->getValue();  
                $medidasAereaOi[4] = $objWorksheet->getCell('E116')->getValue();  
                $medidasAereaOi[5] = $objWorksheet->getCell('E117')->getValue(); 
                $medidasAereaOi[6] = $objWorksheet->getCell('E118')->getValue();  
                $medidasAereaOi[7] = $objWorksheet->getCell('E119')->getValue();  
                $medidasAereaOi[8] = $objWorksheet->getCell('E120')->getValue();  

                //datos de medidasOseaOd
                $medidasOseaOd = array();
                $medidasOseaOd[1] = $objWorksheet->getCell('E123')->getValue(); 
                $medidasOseaOd[2] = $objWorksheet->getCell('E124')->getValue();  
                $medidasOseaOd[3] = $objWorksheet->getCell('E125')->getValue();  
                $medidasOseaOd[4] = $objWorksheet->getCell('E126')->getValue();  
                $medidasOseaOd[5] = $objWorksheet->getCell('E127')->getValue(); 
                $medidasOseaOd[6] = $objWorksheet->getCell('E128')->getValue();  
            }

            //salto a la siguiente hoja del excel
            $i++;

        }
       
        $personas = $em->getRepository('AppBundle:Persona')->findAll();
        //return $this->redirectToRoute('persona_index');
        return $this->render('persona/index.html.twig', array(
            'personas' => $personas,
        ));
    }

}
