<?php 

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AreaRepository extends EntityRepository
{
    /**
     * get personas from an enterprise
     */
    public function findPersonasByArea($areaId)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT p
                FROM AppBundle:Persona p
                    JOIN p.area ar
                        JOIN ar.empresas ae
                        WHERE ae.empresaId = :empresa'
        )->setParameter('empresa', $empresaId);

        return $query->getResult();
    }

    public function getIdEmpresaByPersona($personaId)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT ae.empresaId
                FROM AppBundle:Persona p
                    JOIN p.area ar 
                        JOIN ar.empresas ae
                        WHERE p.personaId = :personaId'
        )->setParameter('personaId', $personaId);

        return $query->getResult();
    }

}