<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empresa
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity
 */
class Empresa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="EMPRESA_NIT", type="integer", nullable=false)
     */
    private $empresaNit;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPRESA_NOMBRE", type="string", length=50, nullable=false)
     */
    private $empresaNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPRESA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $empresaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPRESA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $empresaId;

    /**
     * @ORM\ManyToMany(targetEntity="Area", inversedBy="empresas")
     *
     * @ORM\JoinTable(name="area_empresa",
     *   joinColumns={@ORM\JoinColumn(name="EMPRESA_ID", referencedColumnName="EMPRESA_ID")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="AREA_ID", referencedColumnName="AREA_ID")}
     * )
     *
     **/
    private $areas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->areas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set empresaNit
     *
     * @param integer $empresaNit
     *
     * @return Empresa
     */
    public function setEmpresaNit($empresaNit)
    {
        $this->empresaNit = $empresaNit;

        return $this;
    }

    /**
     * Get empresaNit
     *
     * @return integer
     */
    public function getEmpresaNit()
    {
        return $this->empresaNit;
    }

    /**
     * Set empresaNombre
     *
     * @param string $empresaNombre
     *
     * @return Empresa
     */
    public function setEmpresaNombre($empresaNombre)
    {
        $this->empresaNombre = $empresaNombre;

        return $this;
    }

    /**
     * Get empresaNombre
     *
     * @return string
     */
    public function getEmpresaNombre()
    {
        return $this->empresaNombre;
    }

    /**
     * Set empresaEstado
     *
     * @param string $empresaEstado
     *
     * @return Empresa
     */
    public function setEmpresaEstado($empresaEstado)
    {
        $this->empresaEstado = $empresaEstado;

        return $this;
    }

    /**
     * Get empresaEstado
     *
     * @return string
     */
    public function getEmpresaEstado()
    {
        return $this->empresaEstado;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Add area
     *
     * @param \AppBundle\Entity\Area $area
     *
     * @return Empresa
     */
    public function addArea(\AppBundle\Entity\Area $area)
    {
        $this->areas[] = $area;

        return $this;
    }

    /**
     * Remove area
     *
     * @param \AppBundle\Entity\Area $area
     */
    public function removeArea(\AppBundle\Entity\Area $area)
    {
        $this->areas->removeElement($area);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAreas()
    {
        return $this->areas;
    }
}
