<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario", indexes={@ORM\Index(name="FK_PERSONA_USUARIO", columns={"PERSONA_ID"}), @ORM\Index(name="FK_USUARIO_ROL", columns={"ROL_ID"})})
 * @ORM\Entity
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="USUARIO_NOMBRE", type="string", length=50, nullable=false)
     */
    private $usuarioNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="USUARIO_PASSWORD", type="string", length=50, nullable=false)
     */
    private $usuarioPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="USUARIO_CORREO", type="string", length=50, nullable=false)
     */
    private $usuarioCorreo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="USUARIO_ESTA_ACTIVO", type="boolean", nullable=false)
     */
    private $usuarioEstaActivo;

    /**
     * @var integer
     *
     * @ORM\Column(name="USUARIO_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioId;

    /**
     * @var \AppBundle\Entity\Rol
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ROL_ID", referencedColumnName="ROL_ID")
     * })
     */
    private $rol;

    /**
     * @var \AppBundle\Entity\Persona
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Persona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSONA_ID", referencedColumnName="PERSONA_ID")
     * })
     */
    private $persona;



    /**
     * Set usuarioNombre
     *
     * @param string $usuarioNombre
     *
     * @return Usuario
     */
    public function setUsuarioNombre($usuarioNombre)
    {
        $this->usuarioNombre = $usuarioNombre;

        return $this;
    }

    /**
     * Get usuarioNombre
     *
     * @return string
     */
    public function getUsuarioNombre()
    {
        return $this->usuarioNombre;
    }

    /**
     * Get usuarioNombre
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->usuarioNombre;
    }

    /**
     * Set usuarioPassword
     *
     * @param string $usuarioPassword
     *
     * @return Usuario
     */
    public function setUsuarioPassword($usuarioPassword)
    {
        $this->usuarioPassword = password_hash($usuarioPassword,PASSWORD_DEFAULT);

        return $this;
    }

    /**
     * Get usuarioPassword
     *
     * @return string
     */
    public function getUsuarioPassword()
    {
        return $this->usuarioPassword;
    }

    /**
     * Get usuarioPassword
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->usuarioPassword;
    }

    /**
     * Set usuarioCorreo
     *
     * @param string $usuarioCorreo
     *
     * @return Usuario
     */
    public function setUsuarioCorreo($usuarioCorreo)
    {
        $this->usuarioCorreo = $usuarioCorreo;

        return $this;
    }

    /**
     * Get usuarioCorreo
     *
     * @return string
     */
    public function getUsuarioCorreo()
    {
        return $this->usuarioCorreo;
    }

    /**
     * Set usuarioEstaActivo
     *
     * @param boolean $usuarioEstaActivo
     *
     * @return Usuario
     */
    public function setUsuarioEstaActivo($usuarioEstaActivo)
    {
        $this->usuarioEstaActivo = $usuarioEstaActivo;

        return $this;
    }

    /**
     * Get usuarioEstaActivo
     *
     * @return boolean
     */
    public function getUsuarioEstaActivo()
    {
        return $this->usuarioEstaActivo;
    }

    /**
     * Get usuarioId
     *
     * @return integer
     */
    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    /**
     * Set rol
     *
     * @param \AppBundle\Entity\Rol $rol
     *
     * @return Usuario
     */
    public function setRol(\AppBundle\Entity\Rol $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \AppBundle\Entity\Rol
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Usuario
     */
    public function setPersona(\AppBundle\Entity\Persona $persona = null)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \AppBundle\Entity\Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
    *
    * info asociada a security
    *
    **/
    
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles()
    {
        $rol = $this->rol;
        $rolNombre = $rol->getRolNombre();
        //return array('ROLE_USER');
        return array($rolNombre);
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->usuarioId,
            $this->usuarioNombre,
            $this->usuarioPassword,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->usuarioId,
            $this->usuarioNombre,
            $this->usuarioPassword,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}
