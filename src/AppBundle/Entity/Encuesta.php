<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Encuesta
 *
 * @ORM\Table(name="encuesta")
 * @ORM\Entity
 */
class Encuesta
{
    /**
     * @var string
     *
     * @ORM\Column(name="ENCUESTA_NOMBRE", type="string", length=50, nullable=false)
     */
    private $encuestaNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ENCUESTA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $encuestaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="ENCUESTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $encuestaId;



    /**
     * Set encuestaNombre
     *
     * @param string $encuestaNombre
     *
     * @return Encuesta
     */
    public function setEncuestaNombre($encuestaNombre)
    {
        $this->encuestaNombre = $encuestaNombre;

        return $this;
    }

    /**
     * Get encuestaNombre
     *
     * @return string
     */
    public function getEncuestaNombre()
    {
        return $this->encuestaNombre;
    }

    /**
     * Set encuestaEstado
     *
     * @param string $encuestaEstado
     *
     * @return Encuesta
     */
    public function setEncuestaEstado($encuestaEstado)
    {
        $this->encuestaEstado = $encuestaEstado;

        return $this;
    }

    /**
     * Get encuestaEstado
     *
     * @return string
     */
    public function getEncuestaEstado()
    {
        return $this->encuestaEstado;
    }

    /**
     * Get encuestaId
     *
     * @return integer
     */
    public function getEncuestaId()
    {
        return $this->encuestaId;
    }

    public function __toString(){
        return $this->encuestaNombre;
    }
}
