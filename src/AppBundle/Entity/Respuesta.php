<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Respuesta
 *
 * @ORM\Table(name="respuesta", indexes={@ORM\Index(name="FK_PREGUNTA_RESPUESTA", columns={"POSIBLE_RESPUESTA_ID"})})
 * @ORM\Entity
 */
class Respuesta
{
    /**
     * @var string
     *
     * @ORM\Column(name="RESPUESTA_DESCRIPCION", type="string", length=50, nullable=true)
     */
    private $respuestaDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="RESPUESTA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $respuestaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="RESPUESTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $respuestaId;

    /**
     * @var \AppBundle\Entity\PosiblesRespuestas
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PosiblesRespuestas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSIBLE_RESPUESTA_ID", referencedColumnName="POSIBLE_RESPUESTA_ID")
     * })
     */
    private $posibleRespuesta;

    /**
     * @var \AppBundle\Entity\Pregunta
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PREGUNTA_ID", referencedColumnName="PREGUNTA_ID")
     * })
     */
    private $preguntaId;

    /**
     * @var \AppBundle\Entity\Persona
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Persona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSONA_ID", referencedColumnName="PERSONA_ID")
     * })
     */
    private $personaId;



    /**
     * Set respuestaDescripcion
     *
     * @param string $respuestaDescripcion
     *
     * @return Respuesta
     */
    public function setRespuestaDescripcion($respuestaDescripcion)
    {
        $this->respuestaDescripcion = $respuestaDescripcion;

        return $this;
    }

    /**
     * Get respuestaDescripcion
     *
     * @return string
     */
    public function getRespuestaDescripcion()
    {
        return $this->respuestaDescripcion;
    }

    /**
     * Set respuestaEstado
     *
     * @param string $respuestaEstado
     *
     * @return Respuesta
     */
    public function setRespuestaEstado($respuestaEstado)
    {
        $this->respuestaEstado = $respuestaEstado;

        return $this;
    }

    /**
     * Get respuestaEstado
     *
     * @return string
     */
    public function getRespuestaEstado()
    {
        return $this->respuestaEstado;
    }

    /**
     * Get respuestaId
     *
     * @return integer
     */
    public function getRespuestaId()
    {
        return $this->respuestaId;
    }

    /**
     * Set posibleRespuesta
     *
     * @param \AppBundle\Entity\PosiblesRespuestas $posibleRespuesta
     *
     * @return Respuesta
     */
    public function setPosibleRespuesta(\AppBundle\Entity\PosiblesRespuestas $posibleRespuesta = null)
    {
        $this->posibleRespuesta = $posibleRespuesta;

        return $this;
    }

    /**
     * Get posibleRespuesta
     *
     * @return \AppBundle\Entity\PosiblesRespuestas
     */
    public function getPosibleRespuesta()
    {
        return $this->posibleRespuesta;
    }

    /**
     * Set preguntaId
     *
     * @param \AppBundle\Entity\Pregunta $preguntaId
     *
     * @return Respuesta
     */
    public function setPreguntaId(\AppBundle\Entity\Pregunta $preguntaId = null)
    {
        $this->preguntaId = $preguntaId;

        return $this;
    }

    /**
     * Get preguntaId
     *
     * @return \AppBundle\Entity\Pregunta
     */
    public function getPreguntaId()
    {
        return $this->preguntaId;
    }

    /**
     * Set personaId
     *
     * @param \AppBundle\Entity\Persona $personaId
     *
     * @return Respuesta
     */
    public function setPersonaId(\AppBundle\Entity\Persona $personaId = null)
    {
        $this->personaId = $personaId;

        return $this;
    }

    /**
     * Get personaId
     *
     * @return \AppBundle\Entity\Persona
     */
    public function getPersonaId()
    {
        return $this->personaId;
    }
    
}
