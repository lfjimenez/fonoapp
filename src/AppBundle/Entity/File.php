<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * File
 *
 * @ORM\Entity
 */
class File
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $excelFileId;
    
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Porfavor, subir archivos de tipo excel.")
     * @Assert\File(mimeTypes={ "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" })
     */
    private $excelFile;


    /**
     * Set excelFile
     *
     * @param string $excelFile
     *
     * @return File
     */
    public function setExcelFile($excelFile)
    {
        $this->excelFile = $excelFile;

        return $this;
    }

    /**
     * Get excelFile
     *
     * @return string
     */
    public function getExcelFile()
    {
        return $this->excelFile;
    }

    /**
     * Get excelFileId
     *
     * @return integer
     */
    public function getExcelFileId()
    {
        return $this->excelFileId;
    }
}
