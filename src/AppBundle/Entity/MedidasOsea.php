<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medidas Osea
 *
 * @ORM\Table(name="medidas_osea")
 * @ORM\Entity
 */
class MedidasOsea
{

    /**
     * @var string
     *
     * @ORM\Column(name="MEDIDAS_OSEA_OIDO", type="string", length=10, nullable=false)
     */
    private $medidasOseaOido;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $medidasOseaId;

    /**
     * @var \AppBundle\Entity\Audiometria
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Audiometria", inversedBy="medidasOsea")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AUDIOMETRIA_ID", referencedColumnName="AUDIOMETRIA_ID")
     * })
     */
    private $audiometria;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_250", type="integer")
     *
     */
    private $medidasOsea250;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_500", type="integer")
     *
     */
    private $medidasOsea500;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_1000", type="integer")
     *
     */
    private $medidasOsea1000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_2000", type="integer")
     *
     */
    private $medidasOsea2000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_3000", type="integer")
     *
     */
    private $medidasOsea3000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_OSEA_4000", type="integer")
     *
     */
    private $medidasOsea4000;


    /**
     * Set medidasOseaOido
     *
     * @param string $medidasOseaOido
     *
     * @return MedidasOsea
     */
    public function setMedidasOseaOido($medidasOseaOido)
    {
        $this->medidasOseaOido = $medidasOseaOido;

        return $this;
    }

    /**
     * Get medidasOseaOido
     *
     * @return string
     */
    public function getMedidasOseaOido()
    {
        return $this->medidasOseaOido;
    }

    /**
     * Get medidasOseaId
     *
     * @return integer
     */
    public function getMedidasOseaId()
    {
        return $this->medidasOseaId;
    }

    /**
     * Set medidasOsea250
     *
     * @param integer $medidasOsea250
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea250($medidasOsea250)
    {
        $this->medidasOsea250 = $medidasOsea250;

        return $this;
    }

    /**
     * Get medidasOsea250
     *
     * @return integer
     */
    public function getMedidasOsea250()
    {
        return $this->medidasOsea250;
    }

    /**
     * Set medidasOsea500
     *
     * @param integer $medidasOsea500
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea500($medidasOsea500)
    {
        $this->medidasOsea500 = $medidasOsea500;

        return $this;
    }

    /**
     * Get medidasOsea500
     *
     * @return integer
     */
    public function getMedidasOsea500()
    {
        return $this->medidasOsea500;
    }

    /**
     * Set medidasOsea1000
     *
     * @param integer $medidasOsea1000
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea1000($medidasOsea1000)
    {
        $this->medidasOsea1000 = $medidasOsea1000;

        return $this;
    }

    /**
     * Get medidasOsea1000
     *
     * @return integer
     */
    public function getMedidasOsea1000()
    {
        return $this->medidasOsea1000;
    }

    /**
     * Set medidasOsea2000
     *
     * @param integer $medidasOsea2000
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea2000($medidasOsea2000)
    {
        $this->medidasOsea2000 = $medidasOsea2000;

        return $this;
    }

    /**
     * Get medidasOsea2000
     *
     * @return integer
     */
    public function getMedidasOsea2000()
    {
        return $this->medidasOsea2000;
    }

    /**
     * Set medidasOsea3000
     *
     * @param integer $medidasOsea3000
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea3000($medidasOsea3000)
    {
        $this->medidasOsea3000 = $medidasOsea3000;

        return $this;
    }

    /**
     * Get medidasOsea3000
     *
     * @return integer
     */
    public function getMedidasOsea3000()
    {
        return $this->medidasOsea3000;
    }

    /**
     * Set medidasOsea4000
     *
     * @param integer $medidasOsea4000
     *
     * @return MedidasOsea
     */
    public function setMedidasOsea4000($medidasOsea4000)
    {
        $this->medidasOsea4000 = $medidasOsea4000;

        return $this;
    }

    /**
     * Get medidasOsea4000
     *
     * @return integer
     */
    public function getMedidasOsea4000()
    {
        return $this->medidasOsea4000;
    }

    /**
     * Set audiometria
     *
     * @param \AppBundle\Entity\Audiometria $audiometria
     *
     * @return MedidasOsea
     */
    public function setAudiometria(\AppBundle\Entity\Audiometria $audiometria = null)
    {
        $this->audiometria = $audiometria;

        return $this;
    }

    /**
     * Get audiometria
     *
     * @return \AppBundle\Entity\Audiometria
     */
    public function getAudiometria()
    {
        return $this->audiometria;
    }
}
