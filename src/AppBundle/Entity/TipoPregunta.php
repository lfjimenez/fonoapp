<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoPregunta
 *
 * @ORM\Table(name="tipo_pregunta")
 * @ORM\Entity
 */
class TipoPregunta
{
    /**
     * @var string
     *
     * @ORM\Column(name="TIPO_PREGUNTA_DESCRIPCION", type="string", length=100, nullable=false)
     */
    private $tipoPreguntaDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO_PREGUNTA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $tipoPreguntaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="TIPO_PREGUNTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipoPreguntaId;



    /**
     * Set tipoPreguntaDescripcion
     *
     * @param string $tipoPreguntaDescripcion
     *
     * @return TipoPregunta
     */
    public function setTipoPreguntaDescripcion($tipoPreguntaDescripcion)
    {
        $this->tipoPreguntaDescripcion = $tipoPreguntaDescripcion;

        return $this;
    }

    /**
     * Get tipoPreguntaDescripcion
     *
     * @return string
     */
    public function getTipoPreguntaDescripcion()
    {
        return $this->tipoPreguntaDescripcion;
    }

    /**
     * Set tipoPreguntaEstado
     *
     * @param string $tipoPreguntaEstado
     *
     * @return TipoPregunta
     */
    public function setTipoPreguntaEstado($tipoPreguntaEstado)
    {
        $this->tipoPreguntaEstado = $tipoPreguntaEstado;

        return $this;
    }

    /**
     * Get tipoPreguntaEstado
     *
     * @return string
     */
    public function getTipoPreguntaEstado()
    {
        return $this->tipoPreguntaEstado;
    }

    /**
     * Get tipoPreguntaId
     *
     * @return integer
     */
    public function getTipoPreguntaId()
    {
        return $this->tipoPreguntaId;
    }

    public function __toString(){
        return $this->tipoPreguntaDescripcion;
    }
}
