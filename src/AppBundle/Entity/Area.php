<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Area
 *
 * @ORM\Table(name="area")
 * @ORM\Entity
 */
class Area
{
    /**
     * @var string
     *
     * @ORM\Column(name="AREA_NOMBRE", type="string", length=50, nullable=false)
     */
    private $areaNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="AREA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $areaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="AREA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $areaId;

    /**
     * @ORM\ManyToMany(targetEntity="Empresa", mappedBy="areas")
     *
     **/
    private $empresas;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Persona", mappedBy="area")
     */
    private $personas;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->empresas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set areaNombre
     *
     * @param string $areaNombre
     *
     * @return Area
     */
    public function setAreaNombre($areaNombre)
    {
        $this->areaNombre = $areaNombre;

        return $this;
    }

    /**
     * Get areaNombre
     *
     * @return string
     */
    public function getAreaNombre()
    {
        return $this->areaNombre;
    }

    /**
     * Set areaEstado
     *
     * @param string $areaEstado
     *
     * @return Area
     */
    public function setAreaEstado($areaEstado)
    {
        $this->areaEstado = $areaEstado;

        return $this;
    }

    /**
     * Get areaEstado
     *
     * @return string
     */
    public function getAreaEstado()
    {
        return $this->areaEstado;
    }

    /**
     * Get areaId
     *
     * @return integer
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * Add empresa
     *
     * @param \AppBundle\Entity\Empresa $empresa
     *
     * @return Area
     */
    public function addEmpresa(\AppBundle\Entity\Empresa $empresa)
    {
        $this->empresas[] = $empresa;

        return $this;
    }

    /**
     * Remove empresa
     *
     * @param \AppBundle\Entity\Empresa $empresa
     */
    public function removeEmpresa(\AppBundle\Entity\Empresa $empresa)
    {
        $this->empresas->removeElement($empresa);
    }

    /**
     * Get empresas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmpresas()
    {
        return $this->empresas;
    }

    public function __toString(){
        return $this->areaNombre;
    }

    /**
     * Add persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Area
     */
    public function addPersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas[] = $persona;

        return $this;
    }

    /**
     * Remove persona
     *
     * @param \AppBundle\Entity\Persona $persona
     */
    public function removePersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas->removeElement($persona);
    }

    /**
     * Get personas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonas()
    {
        return $this->personas;
    }
}
