<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonaRespuesta
 *
 * @ORM\Table(name="persona_respuesta")
 * @ORM\Entity
 */
class PersonaRespuesta
{

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSONA_RESPUESTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personaRespuestaId;

    /**
     * @var \AppBundle\Entity\Persona
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Historia", inversedBy="personaRespuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="HISTORIA_ID", referencedColumnName="HISTORIA_ID")
     * })
     */
    private $historia;

    /**
     * @var \AppBundle\Entity\Respuesta
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Respuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RESPUESTA_ID", referencedColumnName="RESPUESTA_ID")
     * })
     */
    private $respuesta;


    /**
     * Get personaRespuestaId
     *
     * @return integer
     */
    public function getPersonaRespuestaId()
    {
        return $this->personaRespuestaId;
    }

    /**
     * Set historia
     *
     * @param \AppBundle\Entity\Historia $historia
     *
     * @return PersonaRespuesta
     */
    public function setHistoria(\AppBundle\Entity\Historia $historia = null)
    {
        $this->historia = $historia;

        return $this;
    }

    /**
     * Get historia
     *
     * @return \AppBundle\Entity\Historia
     */
    public function getHistoria()
    {
        return $this->historia;
    }

    /**
     * Set respuesta
     *
     * @param \AppBundle\Entity\Respuesta $respuesta
     *
     * @return PersonaRespuesta
     */
    public function setRespuesta(\AppBundle\Entity\Respuesta $respuesta = null)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return \AppBundle\Entity\Respuesta
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }
}
