<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PosiblesRespuestas
 *
 * @ORM\Table(name="posibles_respuestas", indexes={@ORM\Index(name="FK_PREGUNTA_POSIBLE_RESPUESTA", columns={"TIPO_PREGUNTA_ID"})})
 * @ORM\Entity
 */
class PosiblesRespuestas
{
    /**
     * @var string
     *
     * @ORM\Column(name="POSIBLE_RESPUESTA_DESCRIPCION", type="string", length=100, nullable=false)
     */
    private $posibleRespuestaDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="POSIBLE_RESPUESTA_ESTADO", type="string", length=1024, nullable=true)
     */
    private $posibleRespuestaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSIBLE_RESPUESTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $posibleRespuestaId;

    /**
     * @var \AppBundle\Entity\TipoPregunta
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoPregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TIPO_PREGUNTA_ID", referencedColumnName="TIPO_PREGUNTA_ID")
     * })
     */
    private $tipoPregunta;



    /**
     * Set posibleRespuestaDescripcion
     *
     * @param string $posibleRespuestaDescripcion
     *
     * @return PosiblesRespuestas
     */
    public function setPosibleRespuestaDescripcion($posibleRespuestaDescripcion)
    {
        $this->posibleRespuestaDescripcion = $posibleRespuestaDescripcion;

        return $this;
    }

    /**
     * Get posibleRespuestaDescripcion
     *
     * @return string
     */
    public function getPosibleRespuestaDescripcion()
    {
        return $this->posibleRespuestaDescripcion;
    }

    /**
     * Set posibleRespuestaEstado
     *
     * @param string $posibleRespuestaEstado
     *
     * @return PosiblesRespuestas
     */
    public function setPosibleRespuestaEstado($posibleRespuestaEstado)
    {
        $this->posibleRespuestaEstado = $posibleRespuestaEstado;

        return $this;
    }

    /**
     * Get posibleRespuestaEstado
     *
     * @return string
     */
    public function getPosibleRespuestaEstado()
    {
        return $this->posibleRespuestaEstado;
    }

    /**
     * Get posibleRespuestaId
     *
     * @return integer
     */
    public function getPosibleRespuestaId()
    {
        return $this->posibleRespuestaId;
    }

    /**
     * Set tipoPregunta
     *
     * @param \AppBundle\Entity\TipoPregunta $tipoPregunta
     *
     * @return PosiblesRespuestas
     */
    public function setTipoPregunta(\AppBundle\Entity\TipoPregunta $tipoPregunta = null)
    {
        $this->tipoPregunta = $tipoPregunta;

        return $this;
    }

    /**
     * Get tipoPregunta
     *
     * @return \AppBundle\Entity\TipoPregunta
     */
    public function getTipoPregunta()
    {
        return $this->tipoPregunta;
    }
}
