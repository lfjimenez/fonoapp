<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Persona
 *
 * @ORM\Table(name="persona", indexes={@ORM\Index(name="FK_PERSONA_AREA", columns={"AREA_ID"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonaRepository")
 */
class Persona
{
    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_NOMBRES", type="string", length=50, nullable=false)
     */
    private $personaNombres;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_APELLIDOS", type="string", length=50, nullable=false)
     */
    private $personaApellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_CEDULA", type="string", length=50, nullable=false)
     */
    private $personaCedula;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_SEXO", type="string", length=10, nullable=false)
     */
    private $personaSexo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PERSONA_FECNAC", type="date", nullable=false)
     */
    private $personaFecnac;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_DIRECCION", type="string", length=50, nullable=true)
     */
    private $personaDireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_BARRIO", type="string", length=50, nullable=true)
     */
    private $personaBarrio;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSONA_TELEFONO", type="integer", nullable=true)
     */
    private $personaTelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="PERSONA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $personaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSONA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personaId;

    /**
     * @var \AppBundle\Entity\Area
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Area", inversedBy="personas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AREA_ID", referencedColumnName="AREA_ID")
     * })
     */
    private $area;

    /**
     * @var \AppBundle\Entity\Empresa
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="EMPRESA_ID", referencedColumnName="EMPRESA_ID")
     * })
     */
    private $empresa;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Audiometria", mappedBy="persona")
     */
    private $audiometrias;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Historia", mappedBy="persona")
     */
    private $historias;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->audiometrias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set personaNombres
     *
     * @param string $personaNombres
     *
     * @return Persona
     */
    public function setPersonaNombres($personaNombres)
    {
        $this->personaNombres = $personaNombres;

        return $this;
    }

    /**
     * Get personaNombres
     *
     * @return string
     */
    public function getPersonaNombres()
    {
        return $this->personaNombres;
    }

    /**
     * Set personaApellidos
     *
     * @param string $personaApellidos
     *
     * @return Persona
     */
    public function setPersonaApellidos($personaApellidos)
    {
        $this->personaApellidos = $personaApellidos;

        return $this;
    }

    /**
     * Get personaApellidos
     *
     * @return string
     */
    public function getPersonaApellidos()
    {
        return $this->personaApellidos;
    }

    /**
     * Set personaCedula
     *
     * @param string $personaCedula
     *
     * @return Persona
     */
    public function setPersonaCedula($personaCedula)
    {
        $this->personaCedula = $personaCedula;

        return $this;
    }

    /**
     * Get personaCedula
     *
     * @return string
     */
    public function getPersonaCedula()
    {
        return $this->personaCedula;
    }

    /**
     * Set personaSexo
     *
     * @param string $personaSexo
     *
     * @return Persona
     */
    public function setPersonaSexo($personaSexo)
    {
        $this->personaSexo = $personaSexo;

        return $this;
    }

    /**
     * Get personaSexo
     *
     * @return string
     */
    public function getPersonaSexo()
    {
        return $this->personaSexo;
    }

    /**
     * Set personaFecnac
     *
     * @param \DateTime $personaFecnac
     *
     * @return Persona
     */
    public function setPersonaFecnac($personaFecnac)
    {
        $this->personaFecnac = $personaFecnac;

        return $this;
    }

    /**
     * Get personaFecnac
     *
     * @return \DateTime
     */
    public function getPersonaFecnac()
    {
        return $this->personaFecnac;
    }

    /**
     * Set personaDireccion
     *
     * @param string $personaDireccion
     *
     * @return Persona
     */
    public function setPersonaDireccion($personaDireccion)
    {
        $this->personaDireccion = $personaDireccion;

        return $this;
    }

    /**
     * Get personaDireccion
     *
     * @return string
     */
    public function getPersonaDireccion()
    {
        return $this->personaDireccion;
    }

    /**
     * Set personaBarrio
     *
     * @param string $personaBarrio
     *
     * @return Persona
     */
    public function setPersonaBarrio($personaBarrio)
    {
        $this->personaBarrio = $personaBarrio;

        return $this;
    }

    /**
     * Get personaBarrio
     *
     * @return string
     */
    public function getPersonaBarrio()
    {
        return $this->personaBarrio;
    }

    /**
     * Set personaTelefono
     *
     * @param integer $personaTelefono
     *
     * @return Persona
     */
    public function setPersonaTelefono($personaTelefono)
    {
        $this->personaTelefono = $personaTelefono;

        return $this;
    }

    /**
     * Get personaTelefono
     *
     * @return integer
     */
    public function getPersonaTelefono()
    {
        return $this->personaTelefono;
    }

    /**
     * Set personaEstado
     *
     * @param string $personaEstado
     *
     * @return Persona
     */
    public function setPersonaEstado($personaEstado)
    {
        $this->personaEstado = $personaEstado;

        return $this;
    }

    /**
     * Get personaEstado
     *
     * @return string
     */
    public function getPersonaEstado()
    {
        return $this->personaEstado;
    }

    /**
     * Get personaId
     *
     * @return integer
     */
    public function getPersonaId()
    {
        return $this->personaId;
    }

    /**
     * Set area
     *
     * @param \AppBundle\Entity\Area $area
     *
     * @return Persona
     */
    public function setArea(\AppBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \AppBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set empresa
     *
     * @param \AppBundle\Entity\Empresa $empresa
     *
     * @return Persona
     */
    public function setEmpresa(\AppBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \AppBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Add audiometria
     *
     * @param \AppBundle\Entity\Audiometria $audiometria
     *
     * @return Persona
     */
    public function addAudiometria(\AppBundle\Entity\Audiometria $audiometria)
    {
        $this->audiometrias[] = $audiometria;

        return $this;
    }

    /**
     * Remove audiometria
     *
     * @param \AppBundle\Entity\Audiometria $audiometria
     */
    public function removeAudiometria(\AppBundle\Entity\Audiometria $audiometria)
    {
        $this->audiometrias->removeElement($audiometria);
    }

    /**
     * Get audiometrias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudiometrias()
    {
        return $this->audiometrias;
    }

    /**
     * Add historia
     *
     * @param \AppBundle\Entity\Historia $historia
     *
     * @return Persona
     */
    public function addHistoria(\AppBundle\Entity\Historia $historia)
    {
        $this->historias[] = $historia;

        return $this;
    }

    /**
     * Remove historia
     *
     * @param \AppBundle\Entity\Historia $historia
     */
    public function removeHistoria(\AppBundle\Entity\Historia $historia)
    {
        $this->historias->removeElement($historia);
    }

    /**
     * Get historias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistorias()
    {
        return $this->historias;
    }
}
