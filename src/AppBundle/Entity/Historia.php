<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historia
 *
 * @ORM\Table(name="historia")
 * @ORM\Entity
 */
class Historia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="HISTORIA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $historiaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HISTORIA_FECHA", type="datetime", nullable=false)
     */
    private $historiaFecha;

    /**
     * @var \AppBundle\Entity\Persona
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Persona", inversedBy="historias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSONA_ID", referencedColumnName="PERSONA_ID")
     * })
     */
    private $persona;

    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PersonaRespuesta", mappedBy="historia")
     */
    private $personaRespuesta;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->historiaFecha = new \DateTime();
        $this->personaRespuesta = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get historiaId
     *
     * @return integer
     */
    public function getHistoriaId()
    {
        return $this->historiaId;
    }

    /**
     * Set historiaFecha
     *
     * @param \DateTime $historiaFecha
     *
     * @return Historia
     */
    public function setHistoriaFecha($historiaFecha)
    {
        $this->historiaFecha = $historiaFecha;

        return $this;
    }

    /**
     * Get historiaFecha
     *
     * @return \DateTime
     */
    public function getHistoriaFecha()
    {
        return $this->historiaFecha;
    }

    /**
     * Set persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Historia
     */
    public function setPersona(\AppBundle\Entity\Persona $persona = null)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \AppBundle\Entity\Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }

    /**
     * Add personaRespuestum
     *
     * @param \AppBundle\Entity\PersonaRespuesta $personaRespuestum
     *
     * @return Historia
     */
    public function addPersonaRespuestum(\AppBundle\Entity\PersonaRespuesta $personaRespuestum)
    {
        $this->personaRespuesta[] = $personaRespuestum;

        return $this;
    }

    /**
     * Remove personaRespuestum
     *
     * @param \AppBundle\Entity\PersonaRespuesta $personaRespuestum
     */
    public function removePersonaRespuestum(\AppBundle\Entity\PersonaRespuesta $personaRespuestum)
    {
        $this->personaRespuesta->removeElement($personaRespuestum);
    }

    /**
     * Get personaRespuesta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonaRespuesta()
    {
        return $this->personaRespuesta;
    }
}
