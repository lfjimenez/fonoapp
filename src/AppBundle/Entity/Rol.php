<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rol
 *
 * @ORM\Table(name="rol")
 * @ORM\Entity
 */
class Rol
{
    /**
     * @var string
     *
     * @ORM\Column(name="ROL_NOMBRE", type="string", length=50, nullable=false)
     */
    private $rolNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ROL_ESTADO", type="string", length=1024, nullable=false)
     */
    private $rolEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="ROL_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rolId;



    /**
     * Set rolNombre
     *
     * @param string $rolNombre
     *
     * @return Rol
     */
    public function setRolNombre($rolNombre)
    {
        $this->rolNombre = $rolNombre;

        return $this;
    }

    /**
     * Get rolNombre
     *
     * @return string
     */
    public function getRolNombre()
    {
        return $this->rolNombre;
    }

    /**
     * Set rolEstado
     *
     * @param string $rolEstado
     *
     * @return Rol
     */
    public function setRolEstado($rolEstado)
    {
        $this->rolEstado = $rolEstado;

        return $this;
    }

    /**
     * Get rolEstado
     *
     * @return string
     */
    public function getRolEstado()
    {
        return $this->rolEstado;
    }

    /**
     * Get rolId
     *
     * @return integer
     */
    public function getRolId()
    {
        return $this->rolId;
    }

    public function __toString(){
        return $this->rolNombre;
    }
}
