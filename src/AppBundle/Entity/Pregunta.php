<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pregunta
 *
 * @ORM\Table(name="pregunta", indexes={@ORM\Index(name="FK_ENCUESTA_PREGUNTA", columns={"ENCUESTA_ID"}), @ORM\Index(name="FK_PREGUNTA_TIPO_PREGUNTA", columns={"TIPO_PREGUNTA_ID"})})
 * @ORM\Entity
 */
class Pregunta
{
    /**
     * @var string
     *
     * @ORM\Column(name="PREGUNTA_DESCRIPCION", type="string", length=300, nullable=false)
     */
    private $preguntaDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="PREGUNTA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $preguntaEstado = 'ACTIVO';

    /**
     * @var integer
     *
     * @ORM\Column(name="PREGUNTA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $preguntaId;

    /**
     * @var \AppBundle\Entity\TipoPregunta
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoPregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TIPO_PREGUNTA_ID", referencedColumnName="TIPO_PREGUNTA_ID")
     * })
     */
    private $tipoPregunta;

    /**
     * @var \AppBundle\Entity\Encuesta
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Encuesta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ENCUESTA_ID", referencedColumnName="ENCUESTA_ID")
     * })
     */
    private $encuesta;



    /**
     * Set preguntaDescripcion
     *
     * @param string $preguntaDescripcion
     *
     * @return Pregunta
     */
    public function setPreguntaDescripcion($preguntaDescripcion)
    {
        $this->preguntaDescripcion = $preguntaDescripcion;

        return $this;
    }

    /**
     * Get preguntaDescripcion
     *
     * @return string
     */
    public function getPreguntaDescripcion()
    {
        return $this->preguntaDescripcion;
    }

    /**
     * Set preguntaEstado
     *
     * @param string $preguntaEstado
     *
     * @return Pregunta
     */
    public function setPreguntaEstado($preguntaEstado)
    {
        $this->preguntaEstado = $preguntaEstado;

        return $this;
    }

    /**
     * Get preguntaEstado
     *
     * @return string
     */
    public function getPreguntaEstado()
    {
        return $this->preguntaEstado;
    }

    /**
     * Get preguntaId
     *
     * @return integer
     */
    public function getPreguntaId()
    {
        return $this->preguntaId;
    }

    /**
     * Set tipoPregunta
     *
     * @param \AppBundle\Entity\TipoPregunta $tipoPregunta
     *
     * @return Pregunta
     */
    public function setTipoPregunta(\AppBundle\Entity\TipoPregunta $tipoPregunta = null)
    {
        $this->tipoPregunta = $tipoPregunta;

        return $this;
    }

    /**
     * Get tipoPregunta
     *
     * @return \AppBundle\Entity\TipoPregunta
     */
    public function getTipoPregunta()
    {
        return $this->tipoPregunta;
    }

    /**
     * Set encuesta
     *
     * @param \AppBundle\Entity\Encuesta $encuesta
     *
     * @return Pregunta
     */
    public function setEncuesta(\AppBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \AppBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }
}
