<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Audiometria
 *
 * @ORM\Table(name="audiometria")
 * @ORM\Entity
 */
class Audiometria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="AUDIOMETRIA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $audiometriaId;

    /**
     * @var string
     *
     * @ORM\Column(name="AUDIOMETRIA_NOMBRE", type="string", length=1024, nullable=false)
     */
    private $audiometriaNombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AUDIOMETRIA_FECHA", type="datetime", nullable=false)
     */
    private $audiometriaFecha;

    /**
     * @var string
     *
     * @ORM\Column(name="AUDIOMETRIA_ESTADO", type="string", length=1024, nullable=false)
     */
    private $audiometriaEstado = 'NORMAL';

    /**
     * @var \AppBundle\Entity\Persona
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Persona", inversedBy="audiometrias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSONA_ID", referencedColumnName="PERSONA_ID")
     * })
     */
    private $persona;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MedidasAerea", mappedBy="audiometria", cascade="persist")
     */
    private $medidasAerea;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MedidasOsea", mappedBy="audiometria", cascade="persist")
     */
    private $medidasOsea;

    /**
     * @var string
     *
     * @ORM\Column(name="AUDIOMETRIA_OTOSCOPIA_OI", type="string", length=1024, nullable=false)
     */
    private $audiometriaOtoscopiaOi;

    /**
     * @var string
     *
     * @ORM\Column(name="AUDIOMETRIA_OTOSCOPIA_OD", type="string", length=1024, nullable=false)
     */
    private $audiometriaOtoscopiaOd;

    /**
     * @var string
     *
     * @ORM\Column(name="AUDIOMETRIA_OBSERVACIONES", type="string", length=1024, nullable=false)
     */
    private $audiometriaObservaciones;

    /**
     * Set audiometriaFecha
     *
     * @param \DateTime $audiometriaFecha
     *
     * @return Audiometria
     */
    public function setAudiometriaFecha($audiometriaFecha)
    {
        $this->audiometriaFecha = $audiometriaFecha;

        return $this;
    }

    /**
     * Get audiometriaFecha
     *
     * @return \DateTime
     */
    public function getAudiometriaFecha()
    {
        return $this->audiometriaFecha;
    }

    /**
     * Set audiometriaEstado
     *
     * @param string $audiometriaEstado
     *
     * @return Audiometria
     */
    public function setAudiometriaEstado($audiometriaEstado)
    {
        $this->audiometriaEstado = $audiometriaEstado;

        return $this;
    }

    /**
     * Get audiometriaEstado
     *
     * @return string
     */
    public function getAudiometriaEstado()
    {
        return $this->audiometriaEstado;
    }

    /**
     * Get audiometriaId
     *
     * @return integer
     */
    public function getAudiometriaId()
    {
        return $this->audiometriaId;
    }

    /**
     * Set persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Audiometria
     */
    public function setPersona(\AppBundle\Entity\Persona $persona = null)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \AppBundle\Entity\Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medidasAerea = new \Doctrine\Common\Collections\ArrayCollection();
        $this->medidasOsea = new \Doctrine\Common\Collections\ArrayCollection();
        $this->audiometriaFecha = new \DateTime();
    }

    /**
     * Add medidasAerea
     *
     * @param \AppBundle\Entity\MedidasAerea $medidasAerea
     *
     * @return Audiometria
     */
    public function addMedidasAerea(\AppBundle\Entity\MedidasAerea $medidasAerea)
    {
        $this->medidasAerea[] = $medidasAerea;

        return $this;
    }

    /**
     * Remove medidasAerea
     *
     * @param \AppBundle\Entity\MedidasAerea $medidasAerea
     */
    public function removeMedidasAerea(\AppBundle\Entity\MedidasAerea $medidasAerea)
    {
        $this->medidasAerea->removeElement($medidasAerea);
    }

    /**
     * Get medidasAerea
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedidasAerea()
    {
        return $this->medidasAerea;
    }

    /**
     * Add medidasOsea
     *
     * @param \AppBundle\Entity\MedidasOsea $medidasOsea
     *
     * @return Audiometria
     */
    public function addMedidasOsea(\AppBundle\Entity\MedidasOsea $medidasOsea)
    {
        $this->medidasOsea[] = $medidasOsea;

        return $this;
    }

    /**
     * Remove medidasOsea
     *
     * @param \AppBundle\Entity\MedidasOsea $medidasOsea
     */
    public function removeMedidasOsea(\AppBundle\Entity\MedidasOsea $medidasOsea)
    {
        $this->medidasOsea->removeElement($medidasOsea);
    }

    /**
     * Get medidasOsea
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedidasOsea()
    {
        return $this->medidasOsea;
    }

    /**
     * Set audiometriaOtoscopiaOi
     *
     * @param string $audiometriaOtoscopiaOi
     *
     * @return Audiometria
     */
    public function setAudiometriaOtoscopiaOi($audiometriaOtoscopiaOi)
    {
        $this->audiometriaOtoscopiaOi = $audiometriaOtoscopiaOi;

        return $this;
    }

    /**
     * Get audiometriaOtoscopiaOi
     *
     * @return string
     */
    public function getAudiometriaOtoscopiaOi()
    {
        return $this->audiometriaOtoscopiaOi;
    }

    /**
     * Set audiometriaOtoscopiaOd
     *
     * @param string $audiometriaOtoscopiaOd
     *
     * @return Audiometria
     */
    public function setAudiometriaOtoscopiaOd($audiometriaOtoscopiaOd)
    {
        $this->audiometriaOtoscopiaOd = $audiometriaOtoscopiaOd;

        return $this;
    }

    /**
     * Get audiometriaOtoscopiaOd
     *
     * @return string
     */
    public function getAudiometriaOtoscopiaOd()
    {
        return $this->audiometriaOtoscopiaOd;
    }

    /**
     * Set audiometriaObservaciones
     *
     * @param string $audiometriaObservaciones
     *
     * @return Audiometria
     */
    public function setAudiometriaObservaciones($audiometriaObservaciones)
    {
        $this->audiometriaObservaciones = $audiometriaObservaciones;

        return $this;
    }

    /**
     * Get audiometriaObservaciones
     *
     * @return string
     */
    public function getAudiometriaObservaciones()
    {
        return $this->audiometriaObservaciones;
    }

    /**
     * Set audiometriaNombre
     *
     * @param string $audiometriaNombre
     *
     * @return Audiometria
     */
    public function setAudiometriaNombre($audiometriaNombre)
    {
        $this->audiometriaNombre = $audiometriaNombre;

        return $this;
    }

    /**
     * Get audiometriaNombre
     *
     * @return string
     */
    public function getAudiometriaNombre()
    {
        return $this->audiometriaNombre;
    }
}
