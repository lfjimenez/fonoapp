<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medidas Aerea
 *
 * @ORM\Table(name="medidas_aerea")
 * @ORM\Entity
 */
class MedidasAerea
{

    /**
     * @var string
     *
     * @ORM\Column(name="MEDIDAS_AEREA_OIDO", type="string", length=10, nullable=false)
     */
    private $medidasAereaOido;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $medidasAereaId;

    /**
     * @var \AppBundle\Entity\Audiometria
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Audiometria", inversedBy="medidasAerea")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AUDIOMETRIA_ID", referencedColumnName="AUDIOMETRIA_ID")
     * })
     */
    private $audiometria;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_250", type="integer")
     *
     */
    private $medidasAerea250;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_500", type="integer")
     *
     */
    private $medidasAerea500;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_1000", type="integer")
     *
     */
    private $medidasAerea1000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_2000", type="integer")
     *
     */
    private $medidasAerea2000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_3000", type="integer")
     *
     */
    private $medidasAerea3000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_4000", type="integer")
     *
     */
    private $medidasAerea4000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_6000", type="integer")
     *
     */
    private $medidasAerea6000;

    /**
     * @var integer
     *
     * @ORM\Column(name="MEDIDAS_AEREA_8000", type="integer")
     *
     */
    private $medidasAerea8000;

    /**
     * @var float
     *
     * @ORM\Column(name="PROMEDIO_PERDIDA", type="float")
     *
     */
    private $promedioPerdida;

    /**
     * @var string
     *
     * @ORM\Column(name="MEDIDAS_AEREA_DIAGNOSTICO", type="string", length=100, nullable=false)
     */
    private $diagnostico;



    /**
     * Set medidasAereaOido
     *
     * @param string $medidasAereaOido
     *
     * @return MedidasAerea
     */
    public function setMedidasAereaOido($medidasAereaOido)
    {
        $this->medidasAereaOido = $medidasAereaOido;

        return $this;
    }

    /**
     * Get medidasAereaOido
     *
     * @return string
     */
    public function getMedidasAereaOido()
    {
        return $this->medidasAereaOido;
    }

    /**
     * Get medidasAereaId
     *
     * @return integer
     */
    public function getMedidasAereaId()
    {
        return $this->medidasAereaId;
    }

    /**
     * Set medidasAerea250
     *
     * @param integer $medidasAerea250
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea250($medidasAerea250)
    {
        $this->medidasAerea250 = $medidasAerea250;

        return $this;
    }

    /**
     * Get medidasAerea250
     *
     * @return integer
     */
    public function getMedidasAerea250()
    {
        return $this->medidasAerea250;
    }

    /**
     * Set medidasAerea500
     *
     * @param integer $medidasAerea500
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea500($medidasAerea500)
    {
        $this->medidasAerea500 = $medidasAerea500;

        return $this;
    }

    /**
     * Get medidasAerea500
     *
     * @return integer
     */
    public function getMedidasAerea500()
    {
        return $this->medidasAerea500;
    }

    /**
     * Set medidasAerea1000
     *
     * @param integer $medidasAerea1000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea1000($medidasAerea1000)
    {
        $this->medidasAerea1000 = $medidasAerea1000;

        return $this;
    }

    /**
     * Get medidasAerea1000
     *
     * @return integer
     */
    public function getMedidasAerea1000()
    {
        return $this->medidasAerea1000;
    }

    /**
     * Set medidasAerea2000
     *
     * @param integer $medidasAerea2000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea2000($medidasAerea2000)
    {
        $this->medidasAerea2000 = $medidasAerea2000;

        return $this;
    }

    /**
     * Get medidasAerea2000
     *
     * @return integer
     */
    public function getMedidasAerea2000()
    {
        return $this->medidasAerea2000;
    }

    /**
     * Set medidasAerea3000
     *
     * @param integer $medidasAerea3000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea3000($medidasAerea3000)
    {
        $this->medidasAerea3000 = $medidasAerea3000;

        return $this;
    }

    /**
     * Get medidasAerea3000
     *
     * @return integer
     */
    public function getMedidasAerea3000()
    {
        return $this->medidasAerea3000;
    }

    /**
     * Set medidasAerea4000
     *
     * @param integer $medidasAerea4000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea4000($medidasAerea4000)
    {
        $this->medidasAerea4000 = $medidasAerea4000;

        return $this;
    }

    /**
     * Get medidasAerea4000
     *
     * @return integer
     */
    public function getMedidasAerea4000()
    {
        return $this->medidasAerea4000;
    }

    /**
     * Set medidasAerea6000
     *
     * @param integer $medidasAerea6000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea6000($medidasAerea6000)
    {
        $this->medidasAerea6000 = $medidasAerea6000;

        return $this;
    }

    /**
     * Get medidasAerea6000
     *
     * @return integer
     */
    public function getMedidasAerea6000()
    {
        return $this->medidasAerea6000;
    }

    /**
     * Set medidasAerea8000
     *
     * @param integer $medidasAerea8000
     *
     * @return MedidasAerea
     */
    public function setMedidasAerea8000($medidasAerea8000)
    {
        $this->medidasAerea8000 = $medidasAerea8000;

        return $this;
    }

    /**
     * Get medidasAerea8000
     *
     * @return integer
     */
    public function getMedidasAerea8000()
    {
        return $this->medidasAerea8000;
    }

    /**
     * Set audiometria
     *
     * @param \AppBundle\Entity\Audiometria $audiometria
     *
     * @return MedidasAerea
     */
    public function setAudiometria(\AppBundle\Entity\Audiometria $audiometria = null)
    {
        $this->audiometria = $audiometria;

        return $this;
    }

    /**
     * Get audiometria
     *
     * @return \AppBundle\Entity\Audiometria
     */
    public function getAudiometria()
    {
        return $this->audiometria;
    }

    /**
     * Set promedioPerdida
     *
     * @param float $promedioPerdida
     *
     * @return MedidasAerea
     */
    public function setPromedioPerdida()
    {
        $promedioPerdida = ($this->medidasAerea500 +
            $this->medidasAerea1000+
            $this->medidasAerea2000+
            $this->medidasAerea3000+
            $this->medidasAerea4000)/5;
        $this->promedioPerdida = $promedioPerdida;

        return $this;
    }

    /**
     * Get promedioPerdida
     *
     * @return float
     */
    public function getPromedioPerdida()
    {
        return $this->promedioPerdida;
    }



    /**
     * Set diagnostico
     *
     * @param string $diagnostico
     *
     * @return MedidasAerea
     */
    public function setDiagnostico()
    {
         if ($this->promedioPerdida>90) {
            $diagnostico = 'Perdida - Hipoacusia profunda';
        }elseif ($this->promedioPerdida>71 && $this->promedioPerdida<=90) {
            $diagnostico = 'Alto riesgo - Hipoacusia severa';
        }elseif ($this->promedioPerdida>41 && $this->promedioPerdida<=55) {
            $diagnostico = 'Alerta - Hipoacusia moderada';
        }elseif ($this->promedioPerdida>26 && $this->promedioPerdida<=40) {
            $diagnostico = 'Preventivo - Hipoacusia leve';
        }elseif ($this->promedioPerdida<=25) {
            $diagnostico = 'Audicion en rango normal';
        }else{
            $this->diagnostico = 'fuera del rango normal';
        }

        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }
}
