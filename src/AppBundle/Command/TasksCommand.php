<?php
namespace AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
 
class TasksCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        //forma de llamar esta tarea: php bin/console fonoapp:taskscommand
        $this->setName('fonoapp:taskscommand')
            ->setDescription('Permite enviar correos de alerta')
            ->addArgument('my_argument', InputArgument::OPTIONAL, 'Explicamos el significado del argumento');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        // Hacemos lo que sea
        //$output->writeln('Hola mundo');
        $em->flush();
    }
}