<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MedidasAereaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('medidasAerea250')
            ->add('medidasAerea500')
            ->add('medidasAerea1000')
            ->add('medidasAerea2000')
            ->add('medidasAerea3000')
            ->add('medidasAerea4000')
            ->add('medidasAerea6000')
            ->add('medidasAerea8000')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\MedidasAerea'
            //'data_class' => null
        ));
    }
}
