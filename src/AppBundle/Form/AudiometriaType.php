<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


class AudiometriaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        /*
            ->add('medidasAerea', MedidasAereaType::Class)
            ->add('medidasOsea', MedidasOseaType::Class)
        ->add('medidasAerea', CollectionType::class, array(
            'entry_type' => MedidasAereaType::class,
            'allow_add'    => true,
            ))
            */
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'AppBundle\Entity\Audiometria'
            'data_class' => null
        ));
    }
}
