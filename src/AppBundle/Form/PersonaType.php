<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PersonaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personaNombres')
            ->add('personaApellidos')
            ->add('personaCedula')
            ->add('personaSexo', ChoiceType::class, array(
                        'choices'  => array(
                            'Masculino' => 'Masculino',
                            'Femenino' => 'Femenino',
                        )
                    )
                )
            ->add('personaFecnac', DateType::class, array(
                'years' => range(date('Y') - 100, date('Y') - 18)
               )
            )
            ->add('personaDireccion')
            ->add('personaBarrio')
            ->add('personaTelefono')
            ->add('personaEstado')
            ->add('area')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Persona'
        ));
    }
}
